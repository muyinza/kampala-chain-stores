package Reports;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.AsyncContext;
import javax.servlet.AsyncEvent;
import javax.servlet.AsyncListener;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;


import org.json.JSONException;
import org.json.JSONObject;


import Beans.Customer;
import Beans.ExcelManagerRow;
import Beans.ExcelOrderRow;
import Beans.ExcelReturnedOrderRow;
import Beans.Manager;
import Beans.Order;
import Beans.Product;
import Beans.Region;
import Beans.ReturnedOrder;
import DatabaseAccess.DatabaseHandler;
import DocumentProcessing.ExcelHandler;
import DocumentProcessing.ExcelProcessingResult;
import DocumentProcessing.TimeHandler;

/**
 * Servlet implementation class HandleOrderExcelReportsServlets
 */
@MultipartConfig
@WebServlet(name="HandleOrderExcelReportsServlets", urlPatterns={"/HandleOrderExcelReportsServlets"},asyncSupported=true)
public class HandleOrderExcelReportsServlets extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HandleOrderExcelReportsServlets() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	    doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	    final DatabaseHandler db_handler = new DatabaseHandler(request.getServletContext());
	    ExcelHandler excel_handler = new ExcelHandler();
	    ExcelProcessingResult excel_processing_result;
	    ArrayList<ExcelOrderRow> list_of_order_rows = new ArrayList<ExcelOrderRow>();
	    ArrayList<ExcelReturnedOrderRow> list_of_returned_orders_rows = new ArrayList<ExcelReturnedOrderRow>();
	    ArrayList<ExcelManagerRow> list_of_managers = new ArrayList<ExcelManagerRow>();
	    Part excel_file = request.getPart("excel_file");
	    String file_name = getFileName(excel_file);
	    InputStream file_content = excel_file.getInputStream();
	    response.setContentType("application/json");
        response.setHeader("Cache-Control", "nocache");
        response.setCharacterEncoding("utf-8");
        final PrintWriter writer = response.getWriter();
	    if(isFileXlsx(file_name))
        {
	        excel_processing_result = excel_handler.processExcelXlsxWorkSheet(file_content);
	        list_of_order_rows = excel_processing_result.getList_of_excel_order_rows();
            list_of_returned_orders_rows = excel_processing_result.getList_of_returned_rows();
            list_of_managers = excel_processing_result.getList_of_manager_rows();
        }
        else if(isFileXls(file_name))
        {
            excel_processing_result = excel_handler.processExcelXlsxWorkSheet(file_content);
            list_of_order_rows = excel_processing_result.getList_of_excel_order_rows();
            list_of_returned_orders_rows = excel_processing_result.getList_of_returned_rows();
            list_of_managers = excel_processing_result.getList_of_manager_rows();
        }
        else
        {
            list_of_order_rows = null;
            list_of_returned_orders_rows = null;
            list_of_managers = null;
        }
	    if(list_of_order_rows == null || list_of_returned_orders_rows == null || list_of_managers == null)
	    {
	        sendJsonResponse("Transaction failed","Invalid File format, Please use .xlsx formats of the latest editions",writer);
	    }
	    else
	    {
	        final ArrayList<ExcelOrderRow> final_list_of_order_rows = list_of_order_rows;
	        final ArrayList<ExcelReturnedOrderRow> final_list_of_returned_orders_rows = list_of_returned_orders_rows;
	        final ArrayList<ExcelManagerRow> final_list_of_managers = list_of_managers; 
	        final AsyncContext async_context = request.startAsync();
	        int async_task_duration = final_list_of_order_rows.size() * 4000;
	        async_context.setTimeout(async_task_duration);
            async_context.addListener(new AsyncListener() {
            
                public void onComplete(AsyncEvent event) throws IOException
                {
                    log("onComplete called");
                }
                public void onTimeout(AsyncEvent event)throws IOException
                {
                    String transaction_status = "Transaction failed";
                    String transaction_description = "Transaction timed out";
                    sendJsonResponse(transaction_status,transaction_description,writer);
                    log("onTimeOut called");
                }
                public void onError(AsyncEvent event)throws IOException
                {
                    log("onError called");
                }
                public void onStartAsync(AsyncEvent event)throws IOException
                {
                    log("onStartAsync called");
                }
            });
            async_context.start(new Runnable() {
                public void run()
                {
                    processAndInsertOrderList(final_list_of_order_rows,db_handler);
                    processAndInsertReturnedOrderList(final_list_of_returned_orders_rows,db_handler);
                    processAndInsertManager(final_list_of_managers,db_handler);
                    sendJsonResponse("Transaction success","Records successfully inserted",writer);
                    async_context.complete();
                }
            });	        
	    }
	}
	
	private void processAndInsertReturnedOrderList(ArrayList<ExcelReturnedOrderRow> list_of_returned_orders_rows,DatabaseHandler db_handler)
    {
	    for(ExcelReturnedOrderRow returned_order_row : list_of_returned_orders_rows)
	    {
	        ReturnedOrder returned_order = returned_order_row.getReturned_order();
	        boolean result_of_registration = registerReturnedOrder(returned_order,db_handler);
	        System.out.println("Result Of registering returned order : "+result_of_registration);
	    }
    }
	
	private void processAndInsertManager(ArrayList<ExcelManagerRow> list_of_managers,DatabaseHandler db_handler)
	{
	    for(ExcelManagerRow manager_row : list_of_managers)
	    {
	        Manager manager = manager_row.getManager();
	        boolean result_of_registration = registerManager(manager,db_handler);
	        System.out.println("Result Of registering manager : "+result_of_registration);
	    }
	}
	
	private boolean registerReturnedOrder(ReturnedOrder returned_order,DatabaseHandler db_handler)
	{
	    String order_id = returned_order.getOrder_id();
	    Order temp_order = db_handler.getOrderFromOrderId(order_id);
	    if(temp_order == null)
	    {
	        return false;
	    }
	    else
	    {
	        return db_handler.insertReturnedOrder(returned_order);
	    }
	}
	
	private boolean registerManager(Manager manager,DatabaseHandler db_handler)
	{
	    String manager_id = new TimeHandler().getCurrentTimeInMilliSeconds();
	    manager.setManager_id(manager_id);
	    String region_name = manager.getregion_id();
	    Region temp_region = db_handler.getRegionFromName(region_name);
	    if(temp_region == null)
	    {	       
	       return(db_handler.insertRegion(new Region(region_name,"n/a")) && db_handler.insertManager(manager));
	    }
	    else
	    {
	        return db_handler.insertManager(manager); 
	    }
	}
	
	private void processAndInsertOrderList(ArrayList<ExcelOrderRow> list_of_order_rows,DatabaseHandler db_handler)
	{
	    for(ExcelOrderRow excel_order_row : list_of_order_rows)
        {
            TimeHandler time_handler = new TimeHandler();
            String customer_id = time_handler.getCurrentTimeInMilliSeconds();
            String product_id = "";
            String region_id = "";
            //wrking on region object
            Region order_region = excel_order_row.getRegion();
            region_id = order_region.getRegion_name();              
            registerRegion(order_region,db_handler);
            //wrking on customer object
            Customer order_customer = excel_order_row.getCustomer();
            order_customer.setCustomer_region_id(region_id);
            order_customer.setCustomer_id(customer_id);
            db_handler.insertCustomer(order_customer);
            //wrking on product object
            Product order_product = excel_order_row.getProduct();
            System.out.println("servlet, is product null : "+(order_product == null));
            product_id = registerProduct(order_product,db_handler);
            //wrking on the order object
            Order order_object = excel_order_row.getOrder();
            order_object.setCustomer_id(customer_id);
            order_object.setProduct_id(product_id);
            order_object.setRegion_id(region_id);
            db_handler.insertOrder(order_object);
        }
	}
	
	private void sendJsonResponse(String transaction_status,String transaction_description,PrintWriter writer)
    {
        try {
            JSONObject json_sent_to_user = new JSONObject();
            json_sent_to_user.put("transaction_status",""+transaction_status);
            json_sent_to_user.put("transaction_description",""+transaction_description);
            writer.println(json_sent_to_user.toString());
            writer.flush();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
	
	private String getFileName(Part part) {
        for (String cd : part.getHeader("content-disposition").split(";")) {
            if (cd.trim().startsWith("filename")) {
                String fileName = cd.substring(cd.indexOf('=') + 1).trim().replace("\"", "");
                return fileName.substring(fileName.lastIndexOf('/') + 1).substring(fileName.lastIndexOf('\\') + 1).trim(); // MSIE fix.
            }
        }
        return null;
    }
	
	private boolean isFileXlsx(String file_name)
    {
        try
        {
            String last_characters = file_name.substring(file_name.trim().length() - 4);
            if(last_characters.equalsIgnoreCase("xlsx"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        catch(Exception exc)
        {
            exc.printStackTrace();
            return false;
        }
    }
	
	private boolean isFileXls(String file_name)
    {
        try
        {
            String last_characters = file_name.substring(file_name.trim().length() - 3);
            if(last_characters.equalsIgnoreCase("xls"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        catch(Exception exc)
        {
            exc.printStackTrace();
            return false;
        }
    }

	private boolean registerRegion(Region region_object,DatabaseHandler db_handler)
	{
	    Region temp_region = db_handler.getRegionFromName(region_object.getRegion_name());
	    if(temp_region == null)
	    {
	        return db_handler.insertRegion(region_object);
	    }
	    else
	    {
	        return true;
	    }
	}
	
	private String registerProduct(Product product_object,DatabaseHandler db_handler)
	{
	    Product temp_product = db_handler.getProductFromDescription(product_object.getProduct_name(),product_object.getProduct_category(),product_object.getProduct_subcategory());
	    if(temp_product == null)
	    {
	        System.out.println("product dint exist");
	        String product_id = new TimeHandler().getCurrentTimeInMilliSeconds();
	        product_object.setProduct_id(product_id);	        
	        System.out.println("Product insertion : "+db_handler.insertProduct(product_object));
	        return product_id;
	    }
	    else
	    {
	        System.out.println("product exists");
	        return temp_product.getProduct_id();
	    }
	}
	
	private void registerOrder()
	{
	    
	}
	
}
