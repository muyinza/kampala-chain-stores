package Reports;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class LogInServlet
 */
@WebServlet("/LogInServlet")
public class LogInServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	String password ;
    String email ;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LogInServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	    doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	    password = request.getParameter("password");
        email = request.getParameter("username");
        if(email.equalsIgnoreCase("Mwangi") && password.equalsIgnoreCase("123456"))
        {
            RequestDispatcher  request_dispatcher = request.getRequestDispatcher("/UploadOrderDetails.jsp");            
            request_dispatcher.forward(request, response);
        }
        else
        {
            RequestDispatcher  request_dispatcher = request.getRequestDispatcher("/index.jsp");
            request.setAttribute("error_message", "Invalid Credentials");
            request_dispatcher.forward(request, response);
        }
	}

}
