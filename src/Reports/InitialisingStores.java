
package Reports;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import DatabaseAccess.DatabaseHandler;

public class InitialisingStores implements ServletContextListener {

    @Override
    public void contextDestroyed(ServletContextEvent arg0) {
        // TODO Auto-generated method stub        
    }

    @Override
    public void contextInitialized(ServletContextEvent arg0) {
        // TODO Auto-generated method stub
        System.out.println("Initialising Kampala Stores");
        DatabaseHandler db_access = new DatabaseHandler(arg0.getServletContext());        
        db_access.initialiseDatabase();
    }

}
