
package DocumentProcessing;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class TimeHandler {
    
    public String getCurrentTimeAndDate()
    {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        return dateFormat.format(date);
    }
                   
    public String getCurrentTimeInMilliSeconds()
    {
        return ""+new Date().getTime();
    }
    
    public String formatStringIntoParsableDate(String date,String format)
    {
        String[] temp_array_of_strings = date.split(" ");
        String final_date_string_month = "";
        String final_date_string_date = "";
        String final_date_string_year = "";        
        if(format.equalsIgnoreCase("dd/mm/yyyy"))
        {
            final_date_string_month = this.getMonthNumber(temp_array_of_strings[0].trim());
            final_date_string_date = temp_array_of_strings[1];
            final_date_string_year = temp_array_of_strings[2];
        }
        return final_date_string_date+"/"+final_date_string_month+"/"+final_date_string_year;
    }
    
    public String getDateStringFromDateObject(Date date)
    {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return dateFormat.format(date);
    }
    
    private String getMonthNumber(String month_name)
    {
        if(month_name.equalsIgnoreCase("January"))
        {
            return "01";
        }
        else if(month_name.equalsIgnoreCase("February"))
        {
            return "02";
        }
        else if(month_name.equalsIgnoreCase("March"))
        {
            return "03";
        }
        else if(month_name.equalsIgnoreCase("April"))
        {
            return "04";
        }
        else if(month_name.equalsIgnoreCase("May"))
        {
            return "05";
        }
        else if(month_name.equalsIgnoreCase("June"))
        {
            return "06";
        }
        else if(month_name.equalsIgnoreCase("July"))
        {
            return "07";
        }
        else if(month_name.equalsIgnoreCase("August"))
        {
            return "08";
        }
        else if(month_name.equalsIgnoreCase("September"))
        {
            return "09";
        }
        else if(month_name.equalsIgnoreCase("October"))
        {
            return "10";
        }
        else if(month_name.equalsIgnoreCase("November"))
        {
            return "11";
        }
        else if(month_name.equalsIgnoreCase("December"))
        {
            return "12";
        }
        else
        {
            return null;
        }
    }
    
    public Calendar getCurrentCalendarDate()
    {                    
            Calendar calendar = Calendar.getInstance();            
            return calendar;               
    }
    
    public Calendar getCalendarObjectFromStringDate(String date_string,String format)
    {           
        Date date;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            date = sdf.parse(date_string);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            return calendar;
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            System.out.println("TimeHandler#getCalendarObjectFromStringDate exception : "+e.getMessage());
            return null;
        }        
    }
    
    public String getDateStringFromCalendar(Calendar calendar_object)
    {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        return dateFormat.format(calendar_object.getTime());
    } 
    
    public String getCurrentDateString()
    {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        return dateFormat.format(Calendar.getInstance().getTime());
    }
    
    public String getDateStringAfterSetDays(int num_of_days)
    {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        Calendar calendar = this.getCalendarAfterSetDays(num_of_days);
        return dateFormat.format(calendar.getTime());
    }
       
    public Calendar getCalendarAfterSetDays(int number_of_days)
    {                
            Calendar calendar = Calendar.getInstance();            
            calendar.add(Calendar.DAY_OF_MONTH,number_of_days);            
            return calendar;                         
    }

    public String getDateAndTimeFromCalendarObject(Calendar calendar_object)
    {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date_object = calendar_object.getTime();
        return dateFormat.format(date_object);
    }

}
