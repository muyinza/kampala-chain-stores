
package DocumentProcessing;

import java.util.ArrayList;

import Beans.ExcelManagerRow;
import Beans.ExcelOrderRow;
import Beans.ExcelReturnedOrderRow;

public class ExcelProcessingResult {
    private ArrayList<ExcelOrderRow> list_of_excel_order_rows;
    private ArrayList<ExcelReturnedOrderRow> list_of_returned_rows;
    private ArrayList<ExcelManagerRow> list_of_manager_rows;
    
    private ExcelProcessingResult()
    {
        
    }
    
    public ExcelProcessingResult(ArrayList<ExcelOrderRow> list_of_excel_order_rows,ArrayList<ExcelReturnedOrderRow> list_of_returned_rows,ArrayList<ExcelManagerRow> list_of_manager_rows)
    {
        this.list_of_excel_order_rows = list_of_excel_order_rows;
        this.list_of_returned_rows = list_of_returned_rows;
        this.list_of_manager_rows = list_of_manager_rows;
    }

    public ArrayList<ExcelOrderRow> getList_of_excel_order_rows() {
        return list_of_excel_order_rows;
    }

    public ArrayList<ExcelReturnedOrderRow> getList_of_returned_rows() {
        return list_of_returned_rows;
    }

    public ArrayList<ExcelManagerRow> getList_of_manager_rows() {
        return list_of_manager_rows;
    }        
    
}
