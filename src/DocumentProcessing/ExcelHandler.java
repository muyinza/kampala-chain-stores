
package DocumentProcessing;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import Beans.ExcelManagerRow;
import Beans.ExcelOrderRow;
import Beans.ExcelReturnedOrderRow;

public class ExcelHandler {    
    
    public ArrayList<ExcelOrderRow> getExcelOrderRowsFromXlsxWorkSheet(InputStream input_stream)
    {
        ArrayList<ExcelOrderRow> list_of_order_rows = new ArrayList<ExcelOrderRow>();
        try
        {            
            XSSFWorkbook workbook = new XSSFWorkbook(input_stream);
            XSSFSheet order_worksheet = workbook.getSheetAt(0);
            //Iterate through each rows one by one
            Iterator<Row> rowIterator = order_worksheet.iterator();
            //to skip the first row
            rowIterator.next();
            while (rowIterator.hasNext())
            {
                Row row = rowIterator.next();
                if(row != null)
                {                    
                    ExcelOrderRow temp_order_row = getExcelOrderRowFromRow(row);
                    list_of_order_rows.add(temp_order_row);
                }
                else
                {
                    continue;
                }
            }
            return list_of_order_rows;
        }
        catch(Exception exc)
        {
            System.out.println("ExcelHandler#getExcelOrderRowsFromXlsxWorkSheet exc : "+exc.getMessage());
            exc.printStackTrace();
            return null;
        }
    }
    
    public ArrayList<ExcelOrderRow> getExcelOrderRowsFromXlsWorkSheet(InputStream input_stream)
    {
        ArrayList<ExcelOrderRow> list_of_order_rows = new ArrayList<ExcelOrderRow>();
        try
        {            
            HSSFWorkbook workbook = new HSSFWorkbook(input_stream);
            HSSFSheet order_worksheet = workbook.getSheetAt(0);
            //Iterate through each rows one by one
            Iterator<Row> rowIterator = order_worksheet.iterator();
            //to skip the first row
            rowIterator.next();
            while (rowIterator.hasNext())
            {
                Row row = rowIterator.next();
                if(row != null)
                {                    
                    ExcelOrderRow temp_order_row = getExcelOrderRowFromRow(row);
                    list_of_order_rows.add(temp_order_row);
                }
                else
                {
                    continue;
                }
            }
            return list_of_order_rows;
        }
        catch(Exception exc)
        {
            System.out.println("ExcelHandler#getExcelOrderRowsFromXlsxWorkSheet exc : "+exc.getMessage());
            exc.printStackTrace();
            return null;
        }
    }
    
    public ArrayList<ExcelOrderRow> getExcelOrderRowsFromXlsxWorkSheet(String file_directory)
    {
        ArrayList<ExcelOrderRow> list_of_order_rows = new ArrayList<ExcelOrderRow>();
        try
        {
            FileInputStream file = new FileInputStream(new File(file_directory));
            XSSFWorkbook workbook = new XSSFWorkbook(file);
            XSSFSheet order_worksheet = workbook.getSheetAt(0);
            //Iterate through each rows one by one
            Iterator<Row> rowIterator = order_worksheet.iterator();
            //to skip the first row
            rowIterator.next();
            while (rowIterator.hasNext())
            {
                Row row = rowIterator.next();
                if(row != null)
                {                    
                    ExcelOrderRow temp_order_row = getExcelOrderRowFromRow(row);
                    list_of_order_rows.add(temp_order_row);
                }
                else
                {
                    continue;
                }
            }
            return list_of_order_rows;
        }
        catch(Exception exc)
        {
            System.out.println("ExcelHandler#getExcelOrderRowsFromXlsxWorkSheet exc : "+exc.getMessage());
            exc.printStackTrace();
            return null;
        }
    }
    
    public ArrayList<ExcelOrderRow> getExcelOrderRowsFromXlsWorkSheet(String file_directory)
    {
        ArrayList<ExcelOrderRow> list_of_order_rows = new ArrayList<ExcelOrderRow>();
        try
        {
            FileInputStream file = new FileInputStream(new File(file_directory));
            HSSFWorkbook workbook = new HSSFWorkbook(file);
            HSSFSheet order_worksheet = workbook.getSheetAt(0);
            //Iterate through each rows one by one
            Iterator<Row> rowIterator = order_worksheet.iterator();
            //to skip the first row
            rowIterator.next();
            while (rowIterator.hasNext())
            {
                Row row = rowIterator.next();
                if(row != null)
                {                    
                    ExcelOrderRow temp_order_row = getExcelOrderRowFromRow(row);
                    list_of_order_rows.add(temp_order_row);
                }
                else
                {
                    continue;
                }
            }
            return list_of_order_rows;
        }
        catch(Exception exc)
        {
            System.out.println("ExcelHandler#getExcelOrderRowsFromXlsxWorkSheet exc : "+exc.getMessage());
            exc.printStackTrace();
            return null;
        }
    }
    
    private ExcelOrderRow getExcelOrderRowFromRow(Row row)
    {
        String excel_row_id = "";
        String excel_order_id = "";
        String excel_order_date = "";
        String excel_order_priority = "";
        String excel_order_quantity = "";
        String excel_sales = "";
        String excel_discount = "";    
        String excel_ship_mode = "";
        String excel_profit = "";
        String excel_unit_price = "";
        String excel_shipping_cost = "";
        String excel_customer_name = "";
        String excel_province = "";
        String excel_region = "";
        String excel_customer_segment = "";
        String excel_product_category = "";
        String excel_product_subcategory = "";
        String excel_product_name = "";
        String excel_product_container = "";
        String excel_product_base_margin = "";
        String excel_shipping_date = "";
        if(row != null)
        {
            Cell row_id_cell = row.getCell(0);
            row_id_cell.setCellType(Cell.CELL_TYPE_STRING);
            excel_row_id = getValueFromCell(row_id_cell);            
            Cell order_id_cell = row.getCell(1);
            order_id_cell.setCellType(Cell.CELL_TYPE_STRING);
            excel_order_id = getValueFromCell(order_id_cell);
            Cell order_date_cell = row.getCell(2);
            Date order_date = order_date_cell.getDateCellValue();
            if(order_date == null)
            {
                excel_order_date = "n/a";
            }
            else
            {
                excel_order_date = ""+new TimeHandler().getDateStringFromDateObject(order_date); 
            }                       
            System.out.println("OrderDateValue : "+excel_order_date);
            Cell order_priority_cell = row.getCell(3);
            excel_order_priority = getValueFromCell(order_priority_cell);
            Cell order_quantity_cell = row.getCell(4);
            excel_order_quantity = getValueFromCell(order_quantity_cell);
            order_quantity_cell.setCellType(Cell.CELL_TYPE_STRING);
            Cell sales_cell = row.getCell(5);
            excel_sales = getValueFromCell(sales_cell);
            Cell discount_cell = row.getCell(6);
            excel_discount = getValueFromCell(discount_cell);
            Cell ship_mode_cell = row.getCell(7);
            excel_ship_mode = getValueFromCell(ship_mode_cell);
            Cell profit_cell = row.getCell(8);
            excel_profit = getValueFromCell(profit_cell);
            Cell unit_price_cell = row.getCell(9);
            excel_unit_price = getValueFromCell(unit_price_cell);
            Cell shipping_cost_cell = row.getCell(10);
            excel_shipping_cost = getValueFromCell(shipping_cost_cell);
            Cell customer_name_cell = row.getCell(11);
            excel_customer_name = getValueFromCell(customer_name_cell);
            Cell province_cell = row.getCell(12);
            excel_province = getValueFromCell(province_cell);
            Cell region_cell = row.getCell(13);
            excel_region = getValueFromCell(region_cell);
            System.out.println("excel region : "+excel_region);
            Cell customer_segment_cell = row.getCell(14);
            excel_customer_segment = getValueFromCell(customer_segment_cell);
            Cell product_category_cell = row.getCell(15);
            excel_product_category = getValueFromCell(product_category_cell);
            Cell product_subcategory_cell = row.getCell(16);
            excel_product_subcategory = getValueFromCell(product_subcategory_cell);
            Cell product_name_cell = row.getCell(17);
            excel_product_name = getValueFromCell(product_name_cell);
            Cell product_container_cell = row.getCell(18);
            excel_product_container = getValueFromCell(product_container_cell);
            Cell product_base_margin_cell = row.getCell(19);
            excel_product_base_margin = getValueFromCell(product_base_margin_cell);
            Cell shipping_date_cell = row.getCell(20);
            if(shipping_date_cell.getDateCellValue() == null)
            {
                excel_shipping_date = "n/a";
            }
            else
            {
                excel_shipping_date = ""+new TimeHandler().getDateStringFromDateObject(shipping_date_cell.getDateCellValue());
            }
            System.out.println("shipping date : "+excel_shipping_date);
            return new ExcelOrderRow(excel_row_id,excel_order_id,excel_order_date,excel_order_priority,excel_order_quantity,excel_sales,excel_discount,excel_ship_mode,excel_profit,excel_unit_price,excel_shipping_cost,excel_customer_name,excel_province,excel_region,excel_customer_segment,excel_product_category,excel_product_subcategory,excel_product_name,excel_product_container,excel_product_base_margin,excel_shipping_date);
        }
        else
        {
            return null;
        }
    }
    
    private String getValueFromCell(Cell cell)
    {
        String temp_value = "";
        if(cell != null)
        {
            switch (cell.getCellType())
            {
                case Cell.CELL_TYPE_NUMERIC:
                    //System.out.print(cell.getNumericCellValue() + "t");
                    temp_value = ""+cell.getNumericCellValue();
                    if(temp_value == null)
                    {
                        return null;
                    }
                    else
                    {
                        return temp_value; 
                    }                
                case Cell.CELL_TYPE_STRING:
                    //System.out.print(cell.getStringCellValue() + "t");
                    temp_value = ""+cell.getStringCellValue();
                    if(temp_value == null)
                    {
                        return null;
                    }
                    else
                    {
                        return temp_value; 
                    }                
            }
            return null;
        }
        else
        {
            return null;
        }        
    }

    public ArrayList<ExcelReturnedOrderRow> getExcelReturnedOrdersFromXlsxWorkSheet(InputStream input_stream)
    {
        ArrayList<ExcelReturnedOrderRow> list_of_order_rows = new ArrayList<ExcelReturnedOrderRow>();
        try
        {            
            XSSFWorkbook workbook = new XSSFWorkbook(input_stream);
            XSSFSheet order_worksheet = workbook.getSheetAt(1);
            //Iterate through each rows one by one
            Iterator<Row> rowIterator = order_worksheet.iterator();
            //to skip the first row
            rowIterator.next();
            while (rowIterator.hasNext())
            {
                Row row = rowIterator.next();
                if(row != null)
                {                    
                    Cell order_id_cell = row.getCell(0);
                    String order_id = getValueFromCell(order_id_cell);
                    Cell return_cell = row.getCell(1);
                    String return_status = getValueFromCell(return_cell);
                    list_of_order_rows.add(new ExcelReturnedOrderRow(order_id,return_status));
                }
                else
                {
                    continue;
                }
            }
            return list_of_order_rows;
        }
        catch(Exception exc)
        {
            System.out.println("ExcelHandler#getExcelOrderRowsFromXlsxWorkSheet exc : "+exc.getMessage());
            exc.printStackTrace();
            return null;
        }
    }
    
    public ArrayList<ExcelReturnedOrderRow> getExcelReturnedOrdersFromXlsWorkSheet(InputStream input_stream)
    {
        ArrayList<ExcelReturnedOrderRow> list_of_order_rows = new ArrayList<ExcelReturnedOrderRow>();
        try
        {            
            HSSFWorkbook workbook = new HSSFWorkbook(input_stream);
            HSSFSheet order_worksheet = workbook.getSheetAt(1);
            //Iterate through each rows one by one
            Iterator<Row> rowIterator = order_worksheet.iterator();
            //to skip the first row
            rowIterator.next();
            while (rowIterator.hasNext())
            {
                Row row = rowIterator.next();
                if(row != null)
                {                    
                    Cell order_id_cell = row.getCell(0);
                    String order_id = getValueFromCell(order_id_cell);
                    Cell return_cell = row.getCell(1);
                    String return_status = getValueFromCell(return_cell);
                    list_of_order_rows.add(new ExcelReturnedOrderRow(order_id,return_status));
                }
                else
                {
                    continue;
                }
            }
            return list_of_order_rows;
        }
        catch(Exception exc)
        {
            System.out.println("ExcelHandler#getExcelOrderRowsFromXlsxWorkSheet exc : "+exc.getMessage());
            exc.printStackTrace();
            return null;
        }
    }
    
    public ArrayList<ExcelReturnedOrderRow> getExcelReturnedOrdersFromXlsxWorkSheet(String file_directory)
    {
        ArrayList<ExcelReturnedOrderRow> list_of_order_rows = new ArrayList<ExcelReturnedOrderRow>();
        try
        {
            FileInputStream file = new FileInputStream(new File(file_directory));
            XSSFWorkbook workbook = new XSSFWorkbook(file);
            XSSFSheet order_worksheet = workbook.getSheetAt(1);
            //Iterate through each rows one by one
            Iterator<Row> rowIterator = order_worksheet.iterator();
            //to skip the first row
            rowIterator.next();
            while (rowIterator.hasNext())
            {
                Row row = rowIterator.next();
                if(row != null)
                {                    
                    Cell order_id_cell = row.getCell(0);
                    String order_id = getValueFromCell(order_id_cell);
                    Cell return_cell = row.getCell(1);
                    String return_status = getValueFromCell(return_cell);
                    list_of_order_rows.add(new ExcelReturnedOrderRow(order_id,return_status));
                }
                else
                {
                    continue;
                }
            }
            return list_of_order_rows;
        }
        catch(Exception exc)
        {
            System.out.println("ExcelHandler#getExcelOrderRowsFromXlsxWorkSheet exc : "+exc.getMessage());
            exc.printStackTrace();
            return null;
        }
    }
    
    public ArrayList<ExcelReturnedOrderRow> getExcelReturnedOrdersFromXlsWorkSheet(String file_directory)
    {
        ArrayList<ExcelReturnedOrderRow> list_of_order_rows = new ArrayList<ExcelReturnedOrderRow>();
        try
        {
            FileInputStream file = new FileInputStream(new File(file_directory));
            HSSFWorkbook workbook = new HSSFWorkbook(file);
            HSSFSheet order_worksheet = workbook.getSheetAt(1);
            //Iterate through each rows one by one
            Iterator<Row> rowIterator = order_worksheet.iterator();
            //to skip the first row
            rowIterator.next();
            while (rowIterator.hasNext())
            {
                Row row = rowIterator.next();
                if(row != null)
                {                    
                    Cell order_id_cell = row.getCell(0);
                    String order_id = getValueFromCell(order_id_cell);
                    Cell return_cell = row.getCell(1);
                    String return_status = getValueFromCell(return_cell);
                    list_of_order_rows.add(new ExcelReturnedOrderRow(order_id,return_status));
                }
                else
                {
                    continue;
                }
            }
            return list_of_order_rows;
        }
        catch(Exception exc)
        {
            System.out.println("ExcelHandler#getExcelOrderRowsFromXlsxWorkSheet exc : "+exc.getMessage());
            exc.printStackTrace();
            return null;
        }
    }

    public ExcelProcessingResult processExcelXlsxWorkSheet(InputStream input_stream)
    {
        ArrayList<ExcelOrderRow> list_of_order_rows = new ArrayList<ExcelOrderRow>();
        ArrayList<ExcelReturnedOrderRow> list_of_returned_order_rows = new ArrayList<ExcelReturnedOrderRow>();
        ArrayList<ExcelManagerRow> list_of_manager_rows = new ArrayList<ExcelManagerRow>();
        try
        {
            XSSFWorkbook workbook = new XSSFWorkbook(input_stream);
            XSSFSheet orders_worksheet = workbook.getSheetAt(0);
            XSSFSheet returned_orders_worksheet = workbook.getSheetAt(1);
            XSSFSheet manager_worksheet = workbook.getSheetAt(2);
            list_of_order_rows = getExcelOrderRowsFromWorkSheet(orders_worksheet);
            list_of_returned_order_rows = getExcelReturnedOrdersFromWorkSheet(returned_orders_worksheet);
            list_of_manager_rows = getExcelManagerRowsFromWorkSheet(manager_worksheet);
            return new ExcelProcessingResult(list_of_order_rows,list_of_returned_order_rows,list_of_manager_rows);
        }
        catch(Exception exc)
        {
            System.out.println("ExcelHandler#processExcelXlsxWorkSheet exc : "+exc.getMessage());
            exc.printStackTrace();
            return null;
        }
    }
    
    public ExcelProcessingResult processExcelXlsWorkSheet(InputStream input_stream)
    {
        ArrayList<ExcelOrderRow> list_of_order_rows = new ArrayList<ExcelOrderRow>();
        ArrayList<ExcelReturnedOrderRow> list_of_returned_order_rows = new ArrayList<ExcelReturnedOrderRow>();
        ArrayList<ExcelManagerRow> list_of_manager_rows = new ArrayList<ExcelManagerRow>();
        try
        {
            HSSFWorkbook workbook = new HSSFWorkbook(input_stream);
            HSSFSheet orders_worksheet = workbook.getSheetAt(0);
            HSSFSheet returned_orders_worksheet = workbook.getSheetAt(1);
            HSSFSheet manager_worksheet = workbook.getSheetAt(2);
            list_of_order_rows = getExcelOrderRowsFromWorkSheet(orders_worksheet);
            list_of_returned_order_rows = getExcelReturnedOrdersFromWorkSheet(returned_orders_worksheet);
            list_of_manager_rows = getExcelManagerRowsFromWorkSheet(manager_worksheet);
            return new ExcelProcessingResult(list_of_order_rows,list_of_returned_order_rows,list_of_manager_rows);
        }
        catch(Exception exc)
        {
            System.out.println("ExcelHandler#processExcelXlsWorkSheet exc : "+exc.getMessage());
            exc.printStackTrace();
            return null;
        }
    }
    
    public ArrayList<ExcelOrderRow> getExcelOrderRowsFromWorkSheet(Sheet order_worksheet)
    {    
        ArrayList<ExcelOrderRow> list_of_order_rows = new ArrayList<ExcelOrderRow>();
        //Iterate through each rows one by one
        Iterator<Row> rowIterator = order_worksheet.iterator();
        //to skip the first row
        rowIterator.next();
        while (rowIterator.hasNext())
        {
            Row row = rowIterator.next();
            if(row != null)
            {                    
                ExcelOrderRow temp_order_row = getExcelOrderRowFromRow(row);
                list_of_order_rows.add(temp_order_row);
            }
            else
            {
                continue;
            }
        }
        return list_of_order_rows;
    }
            
    public ArrayList<ExcelReturnedOrderRow> getExcelReturnedOrdersFromWorkSheet(Sheet order_worksheet)
    {
        ArrayList<ExcelReturnedOrderRow> list_of_order_rows = new ArrayList<ExcelReturnedOrderRow>();
        try
        {
            Iterator<Row> rowIterator = order_worksheet.iterator();
            //to skip the first row
            rowIterator.next();
            while (rowIterator.hasNext())
            {
                Row row = rowIterator.next();
                if(row != null)
                {                    
                    Cell order_id_cell = row.getCell(0);
                    String order_id = getValueFromCell(order_id_cell);
                    Cell return_cell = row.getCell(1);
                    String return_status = getValueFromCell(return_cell);
                    list_of_order_rows.add(new ExcelReturnedOrderRow(order_id,return_status));
                }
                else
                {
                    continue;
                }
            }
            return list_of_order_rows;
        }
        catch(Exception exc)
        {
            System.out.println("ExcelHandler#getExcelReturnedOrdersFromWorkSheet exc : "+exc.getMessage());
            exc.printStackTrace();
            return null;
        }
    }

    public ArrayList<ExcelManagerRow> getExcelManagerRowsFromWorkSheet(Sheet order_worksheet)
    {
        ArrayList<ExcelManagerRow> list_of_manager_rows = new ArrayList<ExcelManagerRow>();
        try
        {
            //Iterate through each rows one by one
            Iterator<Row> rowIterator = order_worksheet.iterator();
            //to skip the first row
            rowIterator.next();
            while (rowIterator.hasNext())
            {
                Row row = rowIterator.next();
                if(row != null)
                {                    
                    Cell region_cell = row.getCell(0);
                    String region_name = getValueFromCell(region_cell);
                    Cell manager_cell = row.getCell(1);
                    String manager_name = getValueFromCell(manager_cell);
                    list_of_manager_rows.add(new ExcelManagerRow(manager_name,region_name));
                }
                else
                {
                    continue;
                }
            }
            return list_of_manager_rows;
        }
        catch(Exception exc)
        {
            System.out.println("ExcelHandler#getExcelManagerRowsFromWorkSheet exc : "+exc.getMessage());
            exc.printStackTrace();
            return null;
        }
    }
}