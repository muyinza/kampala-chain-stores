
package DatabaseAccess;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Properties;
import java.util.Set;

import javax.servlet.ServletContext;

import Beans.Customer;
import Beans.Manager;
import Beans.Order;
import Beans.OrdersDateReport;
import Beans.Product;
import Beans.ProductCategorySalesReport;
import Beans.Region;
import Beans.RegionOrdersList;
import Beans.ReturnedOrder;

public class DatabaseHandler {
    
    final String JDBC_DRIVER       = "com.mysql.jdbc.Driver";
    String DB_URL = "";
    String USER = "";
    String PASS = "";
    private Connection  connection_object = null;
    private Statement   statement_object  = null;
    
  //constructor to instantiate the database class
    public DatabaseHandler(ServletContext servlet_context)
    {
        try
        {
            //reading properties from the properties file
            Properties properties = new Properties();
            properties.load(servlet_context.getResourceAsStream("/WEB-INF/Kampalastores.properties"));
            USER = properties.getProperty("db_username");
            PASS = properties.getProperty("db_password");
            DB_URL = properties.getProperty("db_url");
            //registering the jdbc driver
            Class.forName(JDBC_DRIVER);
        } catch (Exception exc)
        {
            System.out.println("Exception : " + exc.getMessage());
            exc.printStackTrace();
        }
    }
    
    //method to open the connection and create a statement object to execute mysql statments
    public void openConnectionAndCreateStatement()
    {
        try
        {
            //opening a connection and creating the statement object to execute mysql statements
            this.connection_object = DriverManager.getConnection(DB_URL, USER, PASS);
            this.statement_object = connection_object.createStatement();
        } catch (Exception exc)
        {
            System.out.println("Exception : " + exc.getMessage());
            exc.printStackTrace();
        }
    }
   
    //method to close the connection and the statement object   
    public void closeConnection()
    {
        try
        {
            this.connection_object.close();
            this.statement_object.close();
        } catch (Exception exc)
        {
            System.out.println("Exception : " + exc.getMessage());
            exc.printStackTrace();
        }
    }

    //method to initialise the database and create tables
    public boolean initialiseDatabase()
    {
        String sql_to_create_manager_table = "";
        String sql_to_create_region_table = "";
        String sql_to_create_customer_table = "";
        String sql_to_create_product_table = "";
        String sql_to_create_order_table = "";
        String sql_to_create_returns_table = "";
        
        try
        {
            openConnectionAndCreateStatement();
            sql_to_create_region_table = "CREATE TABLE if not exists region_table" +
                    "(id int not null auto_increment primary key, " +
                    " name varchar(255))";
            
            sql_to_create_manager_table = "CREATE TABLE if not exists manager_table" +
                    "(id int not null auto_increment primary key, " +
                    " first_name varchar(255)," +
                    " last_name varchar(255)," +
                    " manager_id varchar(255) unique," +
                    " region_id varchar(255))";
            
            sql_to_create_customer_table = "CREATE TABLE if not exists customer_table" +
                    "(id int not null auto_increment primary key, " +
                    " first_name varchar(255)," +
                    " last_name varchar(255)," +
                    " customer_segment varchar(255)," +
                    " customer_province varchar(255)," +
                    " customer_id varchar(255)," +
                    " customer_region_id varchar(255))";
            
            sql_to_create_product_table = "CREATE TABLE if not exists product_table" +
                    "(id int not null auto_increment primary key, " +                    
                    " product_subcategory varchar(255)," +
                    " product_name varchar(255)," +
                    " product_category varchar(255)," +
                    " product_container varchar(255)," +
                    " product_base_margin varchar(255)," +
                    " product_id varchar(255)," +
                    " UNIQUE (product_name,product_category,product_subcategory))";
            
            sql_to_create_order_table = "CREATE TABLE if not exists order_table" +
                    "(id int not null auto_increment primary key, " +
                    " excel_order_id varchar(255)," +
                    " excel_row_id varchar(255)," +
                    " order_date varchar(255)," +
                    " order_priority varchar(255)," +
                    " order_quantity varchar(255)," +
                    " sales varchar(255)," +
                    " discount varchar(255)," +
                    " ship_mode varchar(255)," +
                    " profit varchar(255)," +
                    " unit_price varchar(255)," +
                    " shipping_cost varchar(255)," +
                    " customer_id varchar(255)," +
                    " ship_date varchar(255)," +
                    " region_id varchar(255)," +
                    " product_id varchar(255))";
            
            sql_to_create_returns_table = "CREATE TABLE if not exists returns_table" +
                    "(id int not null auto_increment primary key, " +
                    " order_id varchar(255)," +
                    " return_status varchar(255))";
            
            System.out.println(sql_to_create_region_table);
            System.out.println(sql_to_create_manager_table);
            System.out.println(sql_to_create_customer_table);
            System.out.println(sql_to_create_product_table);
            System.out.println(sql_to_create_order_table);
            System.out.println(sql_to_create_returns_table);
            
            statement_object.addBatch(sql_to_create_region_table);
            statement_object.addBatch(sql_to_create_manager_table);
            statement_object.addBatch(sql_to_create_customer_table);
            statement_object.addBatch(sql_to_create_product_table);
            statement_object.addBatch(sql_to_create_order_table);
            statement_object.addBatch(sql_to_create_returns_table);
            statement_object.executeBatch();
            return true;
        }
        catch(Exception exc)
        {
            System.out.println("DatabaseException#exc initialise_db : "+exc.getMessage());
            exc.printStackTrace();
            return false;
        }
        finally
        {
            closeConnection();
        }
    }
    
    public boolean insertManager(Manager manager)
    {
        String first_name = manager.getFirst_name();
        String last_name = manager.getLast_name();
        String region_id = manager.getregion_id();
        String manager_id = manager.getManager_id();
        try
        {
            openConnectionAndCreateStatement();
            String sql_statement = "insert into manager_table " +
                    "(first_name,last_name,region_id,manager_id)" +
                    " values('"+first_name+"','"+last_name+"','"+region_id+"','"+manager_id+"');";
            System.out.println("insert manager sql : "+sql_statement);
            int result_of_insertion = statement_object.executeUpdate(sql_statement);
            if (result_of_insertion > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        } catch (Exception exc)
        {
            System.out.println("Exception#Database_access#insertBulkPaymentTransaction : " + exc.getMessage());
            exc.printStackTrace();
            return false;
        } finally
        {
            closeConnection();
        }
    }
    
    public boolean insertRegion(Region region)
    {        
        try
        {
            openConnectionAndCreateStatement();
            System.out.println("region name : "+region.getRegion_name());
            String sql_statement = "insert into region_table " +
                    "(name)" +
                    " values('"+region.getRegion_name()+"')";
            System.out.println("insert region sql : "+sql_statement);
            int result_of_insertion = statement_object.executeUpdate(sql_statement);
            if (result_of_insertion > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        } catch (Exception exc)
        {
            System.out.println("DatabaseException#exc insertRegion : " + exc.getMessage());
            exc.printStackTrace();
            return false;
        } finally
        {
            closeConnection();
        }
    }
    
    public boolean insertCustomer(Customer customer)
    {        
        try
        {
            String first_name = customer.getFirst_name();
            String last_name = customer.getLast_name();
            String customer_segment = customer.getCustomer_segment();
            String customer_province = customer.getCustomer_province();
            String customer_region_id = customer.getCustomer_region_id();
            String customer_id = customer.getCustomer_id();
            openConnectionAndCreateStatement();
            String sql_statement = "insert into customer_table " +
                    "(first_name,last_name,customer_segment,customer_province,customer_region_id,customer_id)" +
                    " values('"+first_name+"','"+last_name+"','"+customer_segment+"','"+customer_province+"','"+customer_region_id+"','"+customer_id+"');";
            int result_of_insertion = statement_object.executeUpdate(sql_statement);
            System.out.println("insert customer sql : "+sql_statement);
            if(result_of_insertion > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        } catch (Exception exc)
        {
            System.out.println("DatabaseException#exc insertCustomer : " + exc.getMessage());
            exc.printStackTrace();
            return false;
        } finally
        {
            closeConnection();
        }
    }
    
    public boolean insertProduct(Product product)
    {        
        try
        {
            String product_category = product.getProduct_category();
            String product_subcategory = product.getProduct_subcategory();
            String product_name = product.getProduct_name();
            String product_container = product.getProduct_container();
            String product_base_margin = product.getProduct_base_margin();
            String product_id = product.getProduct_id();
            openConnectionAndCreateStatement();            
            String sql_statement = "insert into product_table " +
                    "(product_category,product_subcategory,product_name,product_container,product_base_margin,product_id)" +
                    " values('"+product_category+"','"+product_subcategory+"','"+product_name+"','"+product_container+"','"+product_base_margin+"','"+product_id+"');";
            int result_of_insertion = statement_object.executeUpdate(sql_statement);
            System.out.println("insert product sql : "+sql_statement);
            if (result_of_insertion > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        } catch (Exception exc)
        {
            System.out.println("DatabaseException#exc insertProduct : " + exc.getMessage());
            exc.printStackTrace();
            return false;
        } finally
        {
            closeConnection();
        }
    }
    
    public boolean insertOrder(Order order)
    {        
        try
        {
            String excel_row_id = order.getexcel_row_id();
            String excel_order_id = order.getexcel_order_id();
            String order_date = order.getOrder_date();
            String order_priority = order.getOrder_priority();
            String order_quantity = order.getOrder_quantity();
            String sales = order.getSales();
            String discount = order.getDiscount();
            String ship_mode = order.getShip_mode();
            String profit = order.getProfit();
            String unit_price = order.getUnit_price();
            String shipping_cost = order.getShipping_cost();
            String customer_id = order.getCustomer_id();
            String ship_date = order.getship_date();
            String region_id = order.getRegion_id();
            String product_id = order.getProduct_id();
            openConnectionAndCreateStatement();
            String sql_statement = "insert into order_table " +
                    "(excel_order_id,excel_row_id,order_date,order_priority,order_quantity,sales,discount,ship_mode,profit,unit_price,shipping_cost,customer_id,ship_date,region_id,product_id)" +
                    " values('"+excel_order_id+"','"+excel_row_id+"','"+order_date+"','"+order_priority+"','"+order_quantity+"','"+sales+"','"+discount+"','"+ship_mode+"','"+profit+"','"+unit_price+"','"+shipping_cost+"','"+customer_id+"','"+ship_date+"','"+region_id+"','"+product_id+"');";
            int result_of_insertion = statement_object.executeUpdate(sql_statement);
            System.out.println("insert order sql : "+sql_statement);
            if (result_of_insertion > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        } catch (Exception exc)
        {
            System.out.println("DatabaseException#exc insertOrder : " + exc.getMessage());
            exc.printStackTrace();
            return false;
        } finally
        {
            closeConnection();
        }
    }
  
    public boolean insertReturnedOrder(ReturnedOrder returned_order)
    {        
        try
        {
            String order_id = returned_order.getOrder_id();
            String return_status = returned_order.getReturn_status();
            openConnectionAndCreateStatement();            
            String sql_statement = "insert into returns_table " +
                    "(order_id,return_status)" +
                    " values('"+order_id+"','"+return_status+"');";
            int result_of_insertion = statement_object.executeUpdate(sql_statement);
            System.out.println("insert returned order sql : "+sql_statement);
            if (result_of_insertion > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        } catch (Exception exc)
        {
            System.out.println("DatabaseException#exc insertReturnedOrder : " + exc.getMessage());
            exc.printStackTrace();
            return false;
        } finally
        {
            closeConnection();
        }
    }
    
    public ArrayList<ReturnedOrder> getAllReturnedOrders()
    {
        try
        {
            openConnectionAndCreateStatement();
            String sql_statement = "select * from returns_table";
            ResultSet result_set = statement_object.executeQuery(sql_statement);            
            ArrayList<ReturnedOrder> list_of_returned_orders = new ArrayList<ReturnedOrder>();            
            while (result_set.next())
            {
                String order_id = result_set.getString("order_id");
                String return_status = ""+result_set.getInt("return_status");
                list_of_returned_orders.add(new ReturnedOrder(order_id,return_status));
            }
            if (list_of_returned_orders.size() > 0)
            {                
                return list_of_returned_orders;
            }
            else
            {
                return null;
            }
        } catch (Exception exc)
        {
            System.out.println("DatabaseException#exc getAllRegions : " + exc.getMessage());
            return null;
        } finally
        {
            closeConnection();
        }
    }
    
    public ArrayList<Region> getAllRegions()
    {                
        try
        {
            openConnectionAndCreateStatement();
            String sql_statement = "select * from region_table;";
            ResultSet result_set = statement_object.executeQuery(sql_statement);            
            ArrayList<Region> list_of_regions = new ArrayList<Region>();
            while (result_set.next())
            {
                String region_name = result_set.getString("name");
                String region_id = ""+result_set.getInt("id");
                list_of_regions.add(new Region(region_name,region_id));
            }
            if (list_of_regions.size() > 0)
            {                
                return list_of_regions;
            }
            else
            {
                return null;
            }
        } catch (Exception exc)
        {
            System.out.println("DatabaseException#exc getAllRegions : " + exc.getMessage());
            return null;
        } finally
        {
            closeConnection();
        }
    }
    
    public Region getRegionFromName(String input_region_name)
    {
        try
        {
            openConnectionAndCreateStatement();
            String sql_statement = "select * from region_table where name = '"+input_region_name+"';";
            ResultSet result_set = statement_object.executeQuery(sql_statement);
            String region_id = "";
            String region_name = "";
            while (result_set.next())
            {
                region_name = result_set.getString("name");
                region_id = ""+result_set.getInt("id");
            }
            if (region_name.equalsIgnoreCase(input_region_name))
            {                
                return new Region(region_name,region_id);
            }
            else
            {
                return null;
            }
        } catch (Exception exc)
        {
            System.out.println("DatabaseException#exc getRegionFromName : " + exc.getMessage());
            return null;
        } finally
        {
            closeConnection();
        }
    }
    
    public Customer getCustomerFromCustomerID(String id)
    {
        try
        {
            openConnectionAndCreateStatement();
            String sql_statement = "select * from customer_table where id = '"+id+"';";
            ResultSet result_set = statement_object.executeQuery(sql_statement);
            String first_name = "";
            String last_name = "";
            String customer_segment = "";
            String customer_province = "";
            String customer_region_id = "";
            String customer_id = "";
            while (result_set.next())
            {
                first_name = result_set.getString("first_name");
                last_name = result_set.getString("last_name");
                customer_segment = result_set.getString("customer_segment");
                customer_province = result_set.getString("customer_province");
                customer_region_id = result_set.getString("customer_region_id");
                customer_id = result_set.getString("customer_id");
            }
            if (customer_region_id != null)
            {                
                return new Customer(first_name,last_name,customer_segment,customer_province,customer_region_id,customer_id);
            }
            else
            {
                return null;
            }
        } catch (Exception exc)
        {
            System.out.println("DatabaseException#exc getCustomerFromCustomerID : " + exc.getMessage());
            return null;
        } finally
        {
            closeConnection();
        }
    }
    
    public Product getProductFromProductId(String product_id)
    {
        try
        {
            openConnectionAndCreateStatement();            
            String sql_statement = "select * from product_table where product_id = '"+product_id+"'";
            ResultSet result_set = statement_object.executeQuery(sql_statement);
            String product_category = "";
            String product_subcategory = "";
            String product_name = "";
            String product_container = "";
            String product_base_margin = "";            
            while (result_set.next())
            {
                product_category = result_set.getString("product_category");
                product_subcategory = result_set.getString("product_subcategory");
                product_name = result_set.getString("product_name");
                product_container = result_set.getString("product_container");
                product_base_margin = result_set.getString("product_base_margin");
                
            }
            if (product_category != null)
            {                
                return new Product(product_category,product_subcategory,product_name,product_container,product_base_margin,product_id);
            }
            else
            {
                return null;
            }
        } catch (Exception exc)
        {
            System.out.println("DatabaseException#exc getProductFromProductId : " + exc.getMessage());
            return null;
        } finally
        {
            closeConnection();
        }
    }
    
    public Product getProductFromDescription(String product_name,String product_category,String product_subcategory)
    {
        try
        {
            openConnectionAndCreateStatement();
            String sql_statement = "select * from product_table where product_name = '"+product_name+"' and product_category = '"+product_category+"' and product_subcategory = '"+product_subcategory+"'";
            ResultSet result_set = statement_object.executeQuery(sql_statement);            
            String product_container = "";
            String product_base_margin = "";
            String product_id = "";
            while (result_set.next())
            {
                product_category = result_set.getString("product_category");
                product_subcategory = result_set.getString("product_subcategory");
                product_name = result_set.getString("product_name");
                product_container = result_set.getString("product_container");
                product_base_margin = result_set.getString("product_base_margin");
                product_id = ""+result_set.getString("product_id");
            }
            if (product_container.length() > 1)
            {
                return new Product(product_category,product_subcategory,product_name,product_container,product_base_margin,product_id);
            }
            else
            {
                return null;
            }
        } catch (Exception exc)
        {
            System.out.println("DatabaseException#exc getProductFromDescription : " + exc.getMessage());
            return null;
        } finally
        {
            closeConnection();
        }
    }

    public ArrayList<Order> getAllOrders()
    {
        try
        {
            openConnectionAndCreateStatement();
            String sql_statement = "select * from order_table;";
            ResultSet result_set = statement_object.executeQuery(sql_statement);            
            ArrayList<Order> list_of_orders = new ArrayList<Order>();            
            String excel_row_id = ""; 
            String excel_order_id = ""; 
            String order_date = ""; 
            String order_priority = ""; 
            String order_quantity = ""; 
            String sales = ""; 
            String discount = ""; 
            String ship_mode = ""; 
            String profit = ""; 
            String unit_price = ""; 
            String shipping_cost = ""; 
            String customer_id = ""; 
            String ship_date = ""; 
            String region_id = ""; 
            String product_id = ""; 
            while (result_set.next())
            {
                excel_row_id = result_set.getString("excel_row_id");
                excel_order_id = result_set.getString("excel_order_id");
                order_date = result_set.getString("order_date");
                order_priority = result_set.getString("order_priority");
                order_quantity = result_set.getString("order_quantity");
                sales = result_set.getString("sales");
                discount = result_set.getString("discount");
                ship_mode = result_set.getString("ship_mode");
                profit = result_set.getString("profit");
                unit_price = result_set.getString("unit_price");
                shipping_cost = result_set.getString("shipping_cost");
                customer_id = result_set.getString("customer_id");
                ship_date = result_set.getString("ship_date");
                region_id = result_set.getString("region_id");
                product_id = result_set.getString("product_id");
                list_of_orders.add(new Order(excel_row_id,excel_order_id,order_date,order_priority,order_quantity,sales,discount,ship_mode,profit,unit_price,shipping_cost,customer_id,ship_date,region_id,product_id));
            }
            if (list_of_orders.size() > 0)
            {                
                return list_of_orders;
            }
            else
            {
                return null;
            }
        } catch (Exception exc)
        {
            System.out.println("DatabaseException#exc getAllOrders : " + exc.getMessage());
            return null;
        } finally
        {
            closeConnection();
        }
    }
    
    public ReturnedOrder getReturnedOrderFromExcelOrderId(String order_id)
    {
        try
        {
            openConnectionAndCreateStatement();
            String sql_statement = "select * from returns_table where order_id = '"+order_id+"';";
            ResultSet result_set = statement_object.executeQuery(sql_statement);            
            String return_status = "";
            while (result_set.next())
            {
                return_status = result_set.getString("return_status");
            }
            if (return_status != null)
            {                
                return new ReturnedOrder(order_id,return_status);
            }
            else
            {
                return null;
            }
        }
        catch (Exception exc)
        {
            System.out.println("DatabaseException#exc getReturnedOrderFromExcelOrderId : " + exc.getMessage());
            return null;
        }
        finally
        {
            closeConnection();
        }
    }

    public Order getOrderFromOrderId(String order_id)
    {
        try
        {
            openConnectionAndCreateStatement();
            String sql_statement = "select * from order_table where excel_order_id = '"+order_id+"';";
            ResultSet result_set = statement_object.executeQuery(sql_statement);            
            String excel_row_id = ""; 
            String excel_order_id = ""; 
            String order_date = ""; 
            String order_priority = ""; 
            String order_quantity = ""; 
            String sales = ""; 
            String discount = ""; 
            String ship_mode = ""; 
            String profit = ""; 
            String unit_price = ""; 
            String shipping_cost = ""; 
            String customer_id = ""; 
            String ship_date = ""; 
            String region_id = ""; 
            String product_id = ""; 
            while (result_set.next())
            {
                excel_row_id = result_set.getString("excel_row_id");
                excel_order_id = result_set.getString("excel_order_id");
                order_date = result_set.getString("order_date");
                order_priority = result_set.getString("order_priority");
                order_quantity = result_set.getString("order_quantity");
                sales = result_set.getString("sales");
                discount = result_set.getString("discount");
                ship_mode = result_set.getString("ship_mode");
                profit = result_set.getString("profit");
                unit_price = result_set.getString("unit_price");
                shipping_cost = result_set.getString("shipping_cost");
                customer_id = result_set.getString("customer_id");
                ship_date = result_set.getString("ship_date");
                region_id = result_set.getString("region_id");
                product_id = result_set.getString("product_id");
                return new Order(excel_row_id,excel_order_id,order_date,order_priority,order_quantity,sales,discount,ship_mode,profit,unit_price,shipping_cost,customer_id,ship_date,region_id,product_id);
            }
            return null;
        } catch (Exception exc)
        {
            System.out.println("DatabaseException#exc getAllOrders : " + exc.getMessage());
            return null;
        } finally
        {
            closeConnection();
        }
    }
    
    public ArrayList<RegionOrdersList> getOrderPerRegion()
    {
        try
        {
            openConnectionAndCreateStatement();
            ArrayList<RegionOrdersList> list_of_regions_and_orders = new ArrayList<RegionOrdersList>();
            ArrayList<Region> list_of_regions = this.getAllRegions();
            for(Region temp_region : list_of_regions)
            {
                int number_of_orders = getRegionNumberOfOrders(temp_region);
                list_of_regions_and_orders.add(new RegionOrdersList(temp_region.getRegion_name(),""+number_of_orders));
            }
            return list_of_regions_and_orders;
        }
        catch (Exception exc)
        {
            System.out.println("DatabaseException#exc getOrderPerRegion : " + exc.getMessage());
            return null;
        } finally
        {
            closeConnection();
        }
    }
    
    public int getRegionNumberOfOrders(Region region)
    {
        try
        {
            openConnectionAndCreateStatement();
            String sql_statement = "select count(*) from order_table where region_id = '"+region.getRegion_name()+"';";
            ResultSet result_set = statement_object.executeQuery(sql_statement);
            int count_of_rows = 0;
            while (result_set.next())
            {
                count_of_rows = result_set.getInt(1);
            }
            return count_of_rows;
        } catch (Exception exc)
        {
            System.out.println("DatabaseException#exc getNumberRegionNumberOfOrders : " + exc.getMessage());
            return -1;
        } finally
        {
            closeConnection();
        }
    }
        
    public ArrayList<RegionOrdersList> getReturnedOrdersPerRegion()
    {
        try
        {
            openConnectionAndCreateStatement();
            ArrayList<RegionOrdersList> list_of_regions_and_orders = new ArrayList<RegionOrdersList>();
            ArrayList<Region> list_of_regions = this.getAllRegions();
            for(Region temp_region : list_of_regions)
            {
                int number_of_orders = getRegionNumberOfReturnedOrders(temp_region);
                list_of_regions_and_orders.add(new RegionOrdersList(temp_region.getRegion_name(),""+number_of_orders));
            }
            return list_of_regions_and_orders;
        }
        catch (Exception exc)
        {
            System.out.println("DatabaseException#exc getReturnedOrdersPerRegion : " + exc.getMessage());
            return null;
        } finally
        {
            closeConnection();
        }
    }
    
    public int getRegionNumberOfReturnedOrders(Region region)
    {        
        try
        {
            openConnectionAndCreateStatement();
            String sql_statement = "select count(*) from order_table join returns_table on returns_table.order_id = order_table.excel_order_id where region_id = '"+region.getRegion_name()+"';";
            ResultSet result_set = statement_object.executeQuery(sql_statement);
            int count_of_rows = 0;
            while (result_set.next())
            {
                count_of_rows = result_set.getInt(1);
            }
            return count_of_rows;
        } catch (Exception exc)
        {
            System.out.println("DatabaseException#exc getRegionNumberOfReturnedOrders : " + exc.getMessage());
            return -1;
        } finally
        {
            closeConnection();
        }
    }
        
    public String getProductCategorySales(String product_category)
    {
        try
        {
            openConnectionAndCreateStatement();
            String sql_statement = "select sales from order_table join product_table on (product_table.product_id = order_table.product_id) where product_table.product_category = '"+product_category+"';";
            ResultSet result_set = statement_object.executeQuery(sql_statement);            
            String sales = "";
            BigDecimal sales_int = new BigDecimal(0);
            while (result_set.next())
            {
                sales = result_set.getString("sales");
                System.out.println("sent to parser : "+sales);
                BigDecimal temp_variable = getLongFromString(sales);
                System.out.println("recieved from parser : "+temp_variable);
                if(temp_variable != null)
                {
                    sales_int = sales_int.add(temp_variable);
                }
                else
                {
                    continue;
                }
            }
            return ""+sales_int;
        }
        catch (Exception exc)
        {
            System.out.println("DatabaseException#exc getProductSubcategorySales : " + exc.getMessage());
            return null;
        }
        finally
        {
            closeConnection();
        }
    }
    
    private BigDecimal getLongFromString(String text)
    {
        try
        {
            System.out.println("text to be dicimalled : "+text);
            BigDecimal text_int = new BigDecimal(text);
            return text_int;
        }
        catch(Exception exc)
        {
            System.out.println("DatabaseHandler#getLongFromString exc : "+exc);
            exc.printStackTrace();
            return null;
        }
    }
       
    public ArrayList<Product> getAllProducts()
    {
        try
        {
            ArrayList<Product> list_of_product = new ArrayList<Product>();
            openConnectionAndCreateStatement();
            String sql_statement = "select * from product_table";
            ResultSet result_set = statement_object.executeQuery(sql_statement);            
            String product_container = "";
            String product_base_margin = "";
            String product_id = "";
            String product_subcategory = "";
            String product_category = "";
            String product_name = "";
            while (result_set.next())
            {
                product_category = result_set.getString("product_category");
                product_subcategory = result_set.getString("product_subcategory");
                product_name = result_set.getString("product_name");
                product_container = result_set.getString("product_container");
                product_base_margin = result_set.getString("product_base_margin");
                product_id = ""+result_set.getString("product_id");
                list_of_product.add(new Product(product_category,product_subcategory,product_name,product_container,product_base_margin,product_id));
            }
            if (list_of_product.size() > 1)
            {
                return list_of_product;
            }
            else
            {
                return null;
            }
        } catch (Exception exc)
        {
            System.out.println("DatabaseException#exc getAllProducts : " + exc.getMessage());
            return null;
        } finally
        {
            closeConnection();
        } 
    }
    
    private ArrayList<String> getProductCategories()
    {
        ArrayList<Product> list_of_product = this.getAllProducts();
        ArrayList<String> unsorted_list_of_categories = new ArrayList<String>(); 
        for(Product product : list_of_product)
        {
            String temp_category = product.getProduct_category();
            unsorted_list_of_categories.add(temp_category);
        }
        Set<String> hash_set = new HashSet<String>();
        hash_set.addAll(unsorted_list_of_categories);
        unsorted_list_of_categories.clear();
        unsorted_list_of_categories.addAll(hash_set);
        return unsorted_list_of_categories;
    }

    public ArrayList<ProductCategorySalesReport> getProductCategorySalesReport()
    {
        try
        {
            ArrayList<String> list_of_all_categories = this.getProductCategories();
            ArrayList<ProductCategorySalesReport> list_of_reports = new ArrayList<ProductCategorySalesReport>();
            for(String subcategory : list_of_all_categories)
            {
                String sales = this.getProductCategorySales(subcategory);
                list_of_reports.add(new ProductCategorySalesReport(subcategory,sales));
            }
            if(list_of_reports.size() > 0)
            {
                return list_of_reports;
            }
            else
            {
                return null;
            }
        }
        catch(Exception exc)
        {
            System.out.println("DatabaseHandler#getProductCategorySalesReport exc : "+exc.getMessage());
            exc.printStackTrace();
            return null;
        }        
    }
    
    private ArrayList<String> getOrderDates()
    {
        try
        {
            openConnectionAndCreateStatement();
            String sql_statement = "select order_date from order_table order by order_date asc;";
            ResultSet result_set = statement_object.executeQuery(sql_statement);
            ArrayList<String> list_of_dates = new ArrayList<String>();
            String order_date = "";              
            while (result_set.next())
            {
                order_date = result_set.getString("order_date");
                list_of_dates.add(order_date);
            }
            if (list_of_dates.size() > 0)
            {
                return list_of_dates;
            }
            else
            {
                return null;
            }
        } catch (Exception exc)
        {
            System.out.println("DatabaseException#exc getOrderDates : " + exc.getMessage());
            return null;
        } finally
        {
            closeConnection();
        }
    }
    
    public ArrayList<OrdersDateReport> getOrdersDateReport()
    {
        try
        {
            ArrayList<OrdersDateReport> list_of_order_date_reports = new ArrayList<OrdersDateReport>(); 
            openConnectionAndCreateStatement();
            ArrayList<String> list_of_dates = getOrderDates();
            Set<String> hash_set = new HashSet<String>();
            hash_set.addAll(list_of_dates);
            list_of_dates.clear();
            list_of_dates.addAll(hash_set);
            for(String date : list_of_dates)
            {
                String order_count = this.getDateOrdersCount(date);
                list_of_order_date_reports.add(new OrdersDateReport(date,order_count));
            }
            if(list_of_order_date_reports.size() > 0)
            {
                return list_of_order_date_reports;
            }
            else
            {
                return null;
            }
        }
        catch(Exception exc)
        {
            System.out.println("DatabaseException#exc getOrdersDateReport : " + exc.getMessage());
            return null;
        }
        finally
        {
            closeConnection();
        }
    }
    
    private String getDateOrdersCount(String date)
    {
        try
        {
            openConnectionAndCreateStatement();
            String sql_statement = "select count(*) from order_table where order_date = '"+date+"';";
            ResultSet result_set = statement_object.executeQuery(sql_statement);
            int count_of_rows = 0;
            while (result_set.next())
            {
                count_of_rows = result_set.getInt(1);
            }
            return ""+count_of_rows;
        } catch (Exception exc)
        {
            System.out.println("DatabaseException#exc getDateOrdersCount : " + exc.getMessage());
            return null;
        } finally
        {
            closeConnection();
        }
    }
    
    private String getMonthOrderCount(String month)
    {
        try
        {
            openConnectionAndCreateStatement();
            String sql_statement = "select count(*) from order_table where order_date like '%"+month+"%';";
            ResultSet result_set = statement_object.executeQuery(sql_statement);
            int count_of_rows = 0;
            while (result_set.next())
            {
                count_of_rows = result_set.getInt(1);
            }
            return ""+count_of_rows;
        } catch (Exception exc)
        {
            System.out.println("DatabaseException#exc getMonthOrderCount : " + exc.getMessage());
            return null;
        } finally
        {
            closeConnection();
        }
    }

    private ArrayList<String> getMonthsFromDates(ArrayList<String> list_of_dates)
    {
        try
        {
            ArrayList<String> list_of_months = new ArrayList<String>();
            for(String date : list_of_dates)
            {
                String[] date_array = date.split("/");
                String month = date_array[0]+"/"+date_array[1];
                list_of_months.add(month);
            }
            if(list_of_months.size() > 0)
            {
                LinkedHashSet<String> hash_set = new LinkedHashSet<String>();
                hash_set.addAll(list_of_months);
                list_of_months.clear();
                list_of_months.addAll(hash_set);
                return list_of_months;
            }
            else
            {
                return null;
            }
        }
        catch(Exception exc)
        {
            System.out.println("DatabaseException#exc getMonthsFromDates : " + exc.getMessage());
            return null;
        }
    } 
    
    public ArrayList<OrdersDateReport> getOrdersMonthReport()
    {
        try
        {
            ArrayList<OrdersDateReport> list_of_order_date_reports = new ArrayList<OrdersDateReport>(); 
            openConnectionAndCreateStatement();
            ArrayList<String> list_of_dates = getOrderDates();
            LinkedHashSet<String> hash_set = new LinkedHashSet<String>();
            hash_set.addAll(list_of_dates);
            list_of_dates.clear();
            list_of_dates.addAll(hash_set);
            ArrayList<String> list_of_months = this.getMonthsFromDates(list_of_dates);
            for(String month : list_of_months)
            {
                String count_of_orders = this.getMonthOrderCount(month);
                list_of_order_date_reports.add(new OrdersDateReport(month,count_of_orders));
            }
            if(list_of_order_date_reports.size() > 0)
            {
                return list_of_order_date_reports;
            }
            else
            {
                return null;
            }
        }
        catch(Exception exc)
        {
            System.out.println("DatabaseException#exc getOrdersMonthReport : " + exc.getMessage());
            return null;
        }
        finally
        {
            closeConnection();
        }
    }
    
}
