
package Beans;

public class ExcelManagerRow {
    private String manager_name;
    private String region_name;
    private Manager manager;
    
    private ExcelManagerRow()
    {
        
    }
    
    public ExcelManagerRow(String manager_name,String region_name)
    {
        
        this.manager_name = manager_name;
        this.region_name = region_name;
        /*(String manager_id,String region_id,String first_name,String last_name)*/
        manager = new Manager("n/a",region_name,manager_name,manager_name);
    }

    public Manager getManager() {
        return manager;
    }
    
    
    
}
