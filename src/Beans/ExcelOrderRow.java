
package Beans;

public class ExcelOrderRow {
    private String excel_row_id;
    private String excel_order_id;
    private String excel_order_date;
    private String excel_order_priority;
    private String excel_order_quantity;
    private String excel_sales;
    private String excel_discount;    
    private String excel_ship_mode;
    private String excel_profit;
    private String excel_unit_price;
    private String excel_shipping_cost;
    private String excel_customer_name;
    private String excel_province;
    private String excel_region;
    private String excel_customer_segment;
    private String excel_product_category;
    private String excel_product_subcategory;
    private String excel_product_name;
    private String excel_product_container;
    private String excel_product_base_margin;
    private String excel_shipping_date;
    private Region region;
    private Customer customer;
    private Product product;
    private Order order;
    
    private ExcelOrderRow()
    {
        
    }
    
    public ExcelOrderRow(String excel_row_id,String excel_order_id,String excel_order_date,String excel_order_priority,String excel_order_quantity,String excel_sales,String excel_discount,String excel_ship_mode,String excel_profit,String excel_unit_price, String excel_shipping_cost, String excel_customer_name, String excel_province, String excel_region, String excel_customer_segment, String excel_product_category, String excel_product_subcategory, String excel_product_name, String excel_product_container, String excel_product_base_margin, String excel_shipping_date)
    {
        this.excel_row_id = excel_row_id;
        this.excel_order_id = excel_order_id;
        this.excel_order_date = excel_order_date;
        this.excel_order_priority = excel_order_priority;
        this.excel_order_quantity = excel_order_quantity;
        this.excel_sales = excel_sales;
        this.excel_discount = excel_discount;
        this.excel_ship_mode = excel_ship_mode;
        this.excel_profit = excel_profit;
        this.excel_unit_price = excel_unit_price;
        this.excel_shipping_cost = excel_shipping_cost;
        this.excel_customer_name = excel_customer_name;
        this.excel_province = excel_province;
        this.excel_region = excel_region;        
        this.excel_customer_segment = excel_customer_segment;
        this.excel_product_category = excel_product_category;
        this.excel_product_subcategory = excel_product_subcategory;
        this.excel_product_name = excel_product_name;
        this.excel_product_container = excel_product_container;
        this.excel_product_base_margin = excel_product_base_margin;
        this.excel_shipping_date = excel_shipping_date;
        this.region = new Region(excel_region,"n/a");
        System.out.println("excel row constant : "+excel_region);
        String[] array_of_names = excel_customer_name.split("\\s+");
        String customer_first_name = array_of_names[0];
        String customer_last_name = "";
        if(array_of_names.length > 1)
        {
            customer_last_name = array_of_names[1];
        }
        else
        {
            customer_last_name = customer_first_name;
        }
        this.customer = new Customer ("n/a",customer_first_name,customer_last_name,excel_customer_segment,excel_province,excel_region);
        this.product = new Product(excel_product_category,excel_product_subcategory,excel_product_name,excel_product_container,excel_product_base_margin,"n/a");
        this.order = new Order(excel_row_id,excel_order_id,excel_order_date,excel_order_priority,excel_order_quantity,excel_sales,excel_discount,excel_ship_mode,excel_profit,excel_unit_price,excel_shipping_cost,"n/a",excel_shipping_date,"n/a","n/a");
    }

    public Region getRegion() {
        return region;
    }

    public Customer getCustomer() {
        return customer;
    }

    public Product getProduct() {
        return product;
    }

    public Order getOrder() {
        return order;
    }
    
}
