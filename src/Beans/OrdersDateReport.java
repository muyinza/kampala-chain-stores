
package Beans;

public class OrdersDateReport  implements Comparable<OrdersDateReport> {
    private String order_date;
    private String number_of_orders;
    
    private OrdersDateReport()
    {
        
    }
    
    public OrdersDateReport(String order_date,String number_of_orders)
    {
        this.order_date = order_date;
        this.number_of_orders = number_of_orders;
    }

    public String getOrder_date() {
        return order_date;
    }

    public String getNumber_of_orders() {
        return number_of_orders;
    }
    
    public String toString(OrdersDateReport report)
    {
        return ("("+report.getOrder_date()+","+report.getNumber_of_orders()+")");
    }
    
    public int compareTo(OrdersDateReport o) {
        // usually toString should not be used,
        // instead one of the attributes or more in a comparator chain
        return toString().compareTo(o.toString());
    }

}
