
package Beans;

public class RegionOrdersList {
    private String region_name;
    private String number_of_orders;
    
    private RegionOrdersList()
    {
        
    }
    
    public RegionOrdersList(String region_name,String number_of_orders)
    {
        this.region_name = region_name;
        this.number_of_orders = number_of_orders;
    }

    public String getRegion_name() {
        return region_name;
    }

    public String getNumber_of_orders() {
        return number_of_orders;
    }
    
    
    
}
