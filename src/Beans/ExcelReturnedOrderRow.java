
package Beans;

public class ExcelReturnedOrderRow {
    private String order_id;
    private String return_status;
    private ReturnedOrder returned_order;
    
    private ExcelReturnedOrderRow()
    {
        
    }
    
    public ExcelReturnedOrderRow(String order_id,String return_status)
    {
        this.order_id = order_id;
        this.return_status = return_status;
        returned_order = new ReturnedOrder(order_id,return_status);
    }

    public ReturnedOrder getReturned_order() {
        return returned_order;
    }
    
}
