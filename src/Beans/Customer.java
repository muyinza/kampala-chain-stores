
package Beans;

public class Customer {
    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    private String customer_id;
    private String first_name;
    private String last_name;
    private String customer_segment;
    private String customer_province;
    private String customer_region_id;
    
    public void setCustomer_region_id(String customer_region_id) {
        this.customer_region_id = customer_region_id;
    }

    private Customer()
    {
        
    }
    
    public Customer(String customer_id,String first_name,String last_name,String customer_segment,String customer_province,String customer_region_id)
    {
        this.customer_id = customer_id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.customer_segment = customer_segment;
        this.customer_province = customer_province;
        this.customer_region_id = customer_region_id;
    }        

    public String getCustomer_id() {
        return customer_id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getCustomer_segment() {
        return customer_segment;
    }

    public String getCustomer_province() {
        return customer_province;
    }

    public String getCustomer_region_id() {
        return customer_region_id;
    }
        
}
