
package Beans;

public class Region {
    private String region_name;
    private String region_id;    
    
    private Region()
    {
        
    }
    
    public Region(String region_name,String region_id)
    {
        this.region_name = region_name;
        this.region_id = region_id;        
    }        

    public String getRegion_name() {
        return region_name;
    }

    public String getRegion_id() {
        return region_id;
    }

}
