
package Beans;

public class Order {    
    private String excel_row_id;
    private String excel_order_id;
    private String order_date;
    private String order_priority;
    private String order_quantity;
    private String sales;
    private String discount;
    private String ship_mode;
    private String profit;
    private String unit_price;
    private String shipping_cost;
    private String customer_id;
    private String ship_date;
    private String region_id;
    private String product_id;
    
    private Order()
    {
        
    }
    
    public Order(String excel_row_id,String excel_order_id,String order_date,String order_priority,String order_quantity,String sales,String discount,String ship_mode,String profit,String unit_price,String shipping_cost,String customer_id,String ship_date,String region_id,String product_id)
    {        
        this.excel_row_id = excel_row_id;
        this.excel_order_id = excel_order_id;
        this.order_date = order_date;
        this.order_priority = order_priority;
        this.order_quantity = order_quantity;
        this.sales = sales;
        this.discount = discount;
        this.ship_mode = ship_mode;
        this.shipping_cost = shipping_cost;
        this.unit_price = unit_price;
        this.profit = profit;
        this.customer_id = customer_id;
        this.ship_date = ship_date;
        this.region_id = region_id;
        this.product_id = product_id;
    }        

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public void setRegion_id(String region_id) {
        this.region_id = region_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public String getRegion_id() {
        return region_id;
    }

    public String getexcel_row_id() {
        return excel_row_id;
    }

    public String getexcel_order_id() {
        return excel_order_id;
    }

    public String getOrder_date() {
        return order_date;
    }

    public String getOrder_priority() {
        return order_priority;
    }

    public String getOrder_quantity() {
        return order_quantity;
    }

    public String getSales() {
        return sales;
    }

    public String getDiscount() {
        return discount;
    }

    public String getShip_mode() {
        return ship_mode;
    }

    public String getProfit() {
        return profit;
    }

    public String getUnit_price() {
        return unit_price;
    }

    public String getShipping_cost() {
        return shipping_cost;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public String getship_date() {
        return ship_date;
    }            
}
