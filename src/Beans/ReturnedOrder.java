
package Beans;

public class ReturnedOrder {
    private String order_id;
    private String return_status;
    
    private ReturnedOrder()
    {
        
    }
    
    public ReturnedOrder(String order_id,String return_status)
    {
        this.order_id = order_id;
        this.return_status = return_status;
    }

    public String getOrder_id() {
        return order_id;
    }

    public String getReturn_status() {
        return return_status;
    }
            
}
