
package Beans;

public class Manager {
    private String first_name;
    private String last_name;
    private String region_id;
    private String manager_id;
    
    private Manager()
    {
        
    }
    
    public Manager(String manager_id,String region_id,String first_name,String last_name)
    {
        this.first_name = first_name;
        this.last_name = last_name;
        this.region_id = region_id;
        this.manager_id = manager_id;
    }
    
    

    public void setManager_id(String manager_id) {
        this.manager_id = manager_id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getregion_id() {
        return region_id;
    }

    public String getManager_id() {
        return manager_id;
    }

}
