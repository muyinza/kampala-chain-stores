
package Beans;

public class Product {
    private String product_category;
    private String product_subcategory;
    private String product_name;
    private String product_container;
    private String product_base_margin;
    private String product_id;
    
    private Product()
    {
        
    }
    
    public Product(String product_category,String product_subcategory,String product_name,String product_container,String product_base_margin,String product_id)
    {
        this.product_base_margin = product_base_margin;
        this.product_category = product_category;
        this.product_name = product_name;
        this.product_container = product_container;
        this.product_subcategory = product_subcategory;
        this.product_id = product_id;
    }  
    
    

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public String getProduct_category() {
        return product_category;
    }

    public String getProduct_subcategory() {
        return product_subcategory;
    }

    public String getProduct_name() {
        return product_name;
    }

    public String getProduct_container() {
        return product_container;
    }

    public String getProduct_base_margin() {
        return product_base_margin;
    }
            
}
