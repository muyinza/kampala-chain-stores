
package Beans;

public class ProductCategorySalesReport {
    private String product_category;
    private String sales;
    
    private ProductCategorySalesReport()
    {
        
    }
    
    public ProductCategorySalesReport(String product_category,String sales)
    {
        this.product_category = product_category;
        this.sales = sales;        
    }

    public String getProduct_category() {
        return product_category;
    }

    public String getSales() {
        return sales;
    }
    
    
}
