<%@ page import="java.util.*"%>
<%@ page import="DatabaseAccess.*"%>
<%@ page import="Beans.*"%>
<!DOCTYPE html>
<%
 	ArrayList<RegionOrdersList> list_of_regions_and_orders = new DatabaseHandler(getServletContext()).getOrderPerRegion();
%>
<html class="backend">
    <!-- START Head -->
    <head>
        <!-- START META SECTION -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Kampala Stores</title>
        <meta name="author" content="Muyinza Daniel">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../image/touch/apple-touch-icon-144x144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../image/touch/apple-touch-icon-114x114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../image/touch/apple-touch-icon-72x72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="../image/touch/apple-touch-icon-57x57-precomposed.png">
        <link rel="shortcut icon" href="../image/favicon.ico">
        <!--/ END META SECTION -->

        <!-- START STYLESHEETS -->
        <!-- Plugins stylesheet : optional -->
        <!--/ Plugins stylesheet : optional -->

        <!-- Application stylesheet : mandatory -->
        <link rel="stylesheet" href="css/bootstrap.css">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="css/layout.css">
        <link rel="stylesheet" href="css/uielement.css">
        <!--/ Application stylesheet -->

        <!-- Theme stylesheet -->
		<link rel="stylesheet" href="css/theme.css">
        <!--/ Theme stylesheet -->
        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.2/raphael-min.js"></script>
        <link rel="stylesheet" href="css/loadover.css">
        <link rel="stylesheet" href="css/morris.css">
        <link rel="stylesheet" href="css/example.css">
        <script src="js/morris.min.js"></script>		
				
        <!-- modernizr script -->
        <script type="text/javascript" src="js/modernizr.js"></script>
        <!--/ modernizr script -->
        <!-- END STYLESHEETS -->
                
    </head>
    <!--/ END Head -->

    <!-- START Body -->
    <body>
        <!-- START Template Header -->
        <header id="header" class="navbar">
            <!-- START navbar header -->
            <div class="navbar-header">
                <!-- Brand -->
                <a class="navbar-brand" href="javascript:void(0);">
                    <span class="logo-figure"></span>
                    <span class="logo-text">KampalaStores</span>
                </a>
                <!--/ Brand -->
            </div>
            <!--/ END navbar header -->

            <!-- START Toolbar -->
            <div class="navbar-toolbar clearfix">
                <!-- START Left nav -->
                <ul class="nav navbar-nav navbar-left">
                    <!-- Sidebar shrink -->
                    <li class="hidden-xs hidden-sm">
                        <a href="javascript:void(0);" class="sidebar-minimize" data-toggle="minimize" title="Minimize sidebar">
                            <span class="meta">
                                <span class="icon"></span>
                            </span>
                        </a>
                    </li>
                    <!--/ Sidebar shrink -->

                    <!-- Offcanvas left: This menu will take position at the top of template header (mobile only). Make sure that only #header have the `position: relative`, or it may cause unwanted behavior -->
                    <li class="navbar-main hidden-lg hidden-md hidden-sm">
                        <a href="javascript:void(0);" data-toggle="sidebar" data-direction="ltr" rel="tooltip" title="Menu sidebar">
                            <span class="meta">
                                <span class="icon"><i class="ico-paragraph-justify3"></i></span>
                            </span>
                        </a>
                    </li>
                    <!--/ Offcanvas left -->

					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Shortcuts</a>
							<ul class="dropdown-menu">
								<li><a href="#">Register Manager</a></li>
								<li><a href="#">Register Product</a></li>								
								<li><a href="#">Register Region</a></li>
							</ul>
					</li>
					
                </ul>
                <!--/ END Left nav -->

                <!-- START navbar form -->
                <!-- <div class="navbar-form navbar-left dropdown" id="dropdown-form">
                    <form action="" role="search">
                        <div class="has-icon">
                            <input type="text" class="form-control" placeholder="Search application...">
                            <i class="ico-search form-control-icon"></i>
                        </div>
                    </form>
                </div> -->
                <!-- START navbar form -->

                <!-- START Right nav -->
                <ul class="nav navbar-nav navbar-right">					

                    <!-- Profile dropdown -->
                    <li class="dropdown profile">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                            <span class="meta">
                                <span class="avatar"><img src="../image/avatar/avatar7.jpg" class="img-circle" alt="" /></span>
                                <span class="text hidden-xs hidden-sm pl5">Admin Settings</span>
                            </span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="javascript:void(0);"><span class="icon"><i class="ico-user-plus2"></i></span> My Accounts</a></li>
                            <li><a href="javascript:void(0);"><span class="icon"><i class="ico-cog4"></i></span> Profile Setting</a></li>
                            <li><a href="javascript:void(0);"><span class="icon"><i class="ico-question"></i></span> Help</a></li>                            
                            <li><a href="javascript:void(0);"><span class="icon"><i class="ico-exit"></i></span> Sign Out</a></li>
                        </ul>
                    </li>
                  
                </ul>
                <!--/ END Right nav -->
            </div>
            <!--/ END Toolbar -->
        </header>
        <!--/ END Template Header -->

        <!-- START Template Sidebar (Left) -->
        <aside class="sidebar sidebar-left sidebar-menu">
			<!-- START Sidebar Content -->
            <section class="content slimscroll">
				<!-- START Template Navigation/Menu -->
                <ul class="topmenu topmenu-responsive" data-toggle="menu">                    
                    <li>
                        <a href="UploadOrderDetails.jsp" data-target="#dashboard" data-parent=".topmenu">
                            <span class="figure"><i class="ico-home2"></i></span>
                            <span class="text">Upload Report</span>
                        </a>
                    </li>
                    <li class="active open">
                        <a href="OrdersSummaryPage.jsp" data-target="#dashboard" data-parent=".topmenu">
                            <span class="figure"><i class="ico-stats-up"></i></span>
                            <span class="text">Orders Per Region</span>
                        </a>
                    </li>
                    <li>
                        <a href="ReturnsPerRegion.jsp" data-target="#dashboard" data-parent=".topmenu">
                            <span class="figure"><i class="ico-stats-up"></i></span>
                            <span class="text">Returns Per Region</span>
                        </a>
                    </li>
                    <li>
                        <a href="ProductCategorySalesReportPage.jsp" data-target="#dashboard" data-parent=".topmenu">
                            <span class="figure"><i class="ico-stats-up"></i></span>
                            <span class="text">Product Category Sales</span>
                        </a>
                    </li>
                    <li>
                        <a href="OrdersPerDatePage.jsp" data-target="#dashboard" data-parent=".topmenu">
                            <span class="figure"><i class="ico-stats-up"></i></span>
                            <span class="text">Orders Per Month</span>
                        </a>
                    </li>				
				</ul>
                <!--/ END Template Navigation/Menu -->
            </section>
            <!--/ END Sidebar Container -->
        </aside>
        <!--/ END Template Sidebar (Left) -->
				
        <!-- START Template Main -->
        <section id="main" role="main">
            <!-- START Template Container -->
            <div class="container-fluid">
                <!-- Page Header -->
                <div class="page-header page-header-block">
                    <div class="page-header-section">
                        <h4 class="title semibold">Orders Data</h4>
                    </div>
                    <div class="page-header-section">
                        <!-- Toolbar -->
                        <div class="toolbar">
                            <ol class="breadcrumb breadcrumb-transparent nm">
                                <li><a href="#">Reports</a></li>
                                <li class="active">Orders Per Region</li>
                            </ol>
                        </div>
                        <!--/ Toolbar -->
                    </div>
                </div>
                <!-- Page Header -->
                <div class="row">
            	<div class="col-md-12">
            		<div class="panel panel-info">
            			<div class="panel-heading">
                                <h3 class="panel-title">Orders Per Region</h3>
                        </div>
                        <div class="panel-body">
                        	<div id="orders_per_region_graph"></div>
                        </div>
            		</div>
            	</div>
            </div>
            
            <div class="row">
            	<div class="col-md-12">
            		<div class="panel panel-info">
            			<div class="panel-heading">
                                <h3 class="panel-title">Orders Per Region</h3>
                        </div>
                        <div class="panel-body">
                        	<div id="orders_per_region_donut"></div>
                        </div>
            		</div>
            	</div>
            </div>
                            
            </div>
            <!--/ END Template Container -->
            <!-- START To Top Scroller -->
            <a href="#" class="totop animation" data-toggle="waypoints totop" data-showanim="bounceIn" data-hideanim="bounceOut" data-offset="50%"><i class="ico-angle-up"></i></a>
            <!--/ END To Top Scroller -->
            
        </section>
        <!--/ END Template Main -->

        <!-- START JAVASCRIPT SECTION (Load javascripts at bottom to reduce load time) -->
        <!-- Application and vendor script : mandatory -->
        <script type="text/javascript" src="js/vendor.js"></script>
        <script type="text/javascript" src="js/core.js"></script>
        <script type="text/javascript" src="js/app.js"></script>
        <!-- <script src="js/jQuery-2.1.4.min.js" type="text/javascript"></script> -->
        <script src="js/loadover.js"></script>
        <!--/ Application and vendor script : mandatory -->

        <!-- Plugins and page level script : optional -->
		<script type="text/javascript" src="js/pace.min.js"></script>
        <!--/ Plugins and page level script : optional -->
        <!--/ END JAVASCRIPT SECTION -->
    
    <script type="text/javascript">
 // Use Morris.Bar
    Morris.Bar({
      element: 'orders_per_region_graph',
      data: [
             <%
             	if(list_of_regions_and_orders != null)
             	{
             	   String data_item = "";
             	   for(RegionOrdersList region_order : list_of_regions_and_orders)
                	{
                	   data_item += "{Orders: '"+region_order.getRegion_name()+"', Region: "+region_order.getNumber_of_orders()+"},"; 
                	}
             	  	data_item = data_item.trim().substring(0, data_item.length()-1);
             	  	out.println(data_item);
             	}             	
             %>        
      ],
      xkey: 'Orders',
      ykeys: ['Region'],
      labels: ['Orders']
    });
 
    Morris.Donut({
        element: 'orders_per_region_donut',
        data: [
               <%
               	if(list_of_regions_and_orders != null)
               	{
               	   String data_item = "";
               	   for(RegionOrdersList region_order : list_of_regions_and_orders)
                  	{
                  	   data_item += "{label: '"+region_order.getRegion_name()+"', value: "+region_order.getNumber_of_orders()+"},"; 
                  	}
               	  	data_item = data_item.trim().substring(0, data_item.length()-1);
               	  	out.println(data_item);
               	}             	
               %>        
        ]
      });
	</script>
    
    </body>
    <!--/ END Body -->
</html>