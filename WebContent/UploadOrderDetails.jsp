
<!DOCTYPE html>
<html class="backend">
    <!-- START Head -->
    <head>
        <!-- START META SECTION -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Kampala Stores</title>
        <meta name="author" content="Muyinza Daniel">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../image/touch/apple-touch-icon-144x144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../image/touch/apple-touch-icon-114x114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../image/touch/apple-touch-icon-72x72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="../image/touch/apple-touch-icon-57x57-precomposed.png">
        <link rel="shortcut icon" href="../image/favicon.ico">
        <!--/ END META SECTION -->

        <!-- START STYLESHEETS -->
        <!-- Plugins stylesheet : optional -->
        <!--/ Plugins stylesheet : optional -->

        <!-- Application stylesheet : mandatory -->
        <link rel="stylesheet" href="css/bootstrap.css">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="css/layout.css">
        <link rel="stylesheet" href="css/uielement.css">
        <!--/ Application stylesheet -->

        <!-- Theme stylesheet -->
		<link rel="stylesheet" href="css/theme.css">
        <!--/ Theme stylesheet -->
        
        <link rel="stylesheet" href="css/loadover.css">
		<link rel="stylesheet" href="css/nav-wizard.bootstrap.css">
		
		<style type="text/css">
.btn-file {
  position: relative;
  overflow: hidden;
}
.btn-file input[type=file] {
  position: absolute;
  top: 0;
  right: 0;
  min-width: 100%;
  min-height: 100%;
  font-size: 100px;
  text-align: right;
  filter: alpha(opacity=0);
  opacity: 0;
  background: red;
  cursor: inherit;
  display: block;
}
input[readonly] {
  background-color: white !important;
  cursor: text !important;
}
</style>

        <!-- modernizr script -->
        <script type="text/javascript" src="js/modernizr.js"></script>
        <!--/ modernizr script -->
        <!-- END STYLESHEETS -->
                
    </head>
    <!--/ END Head -->

    <!-- START Body -->
    <body>
        <!-- START Template Header -->
        <header id="header" class="navbar">
            <!-- START navbar header -->
            <div class="navbar-header">
                <!-- Brand -->
                <a class="navbar-brand" href="javascript:void(0);">
                    <span class="logo-figure"></span>
                    <span class="logo-text">KampalaStores</span>
                </a>
                <!--/ Brand -->
            </div>
            <!--/ END navbar header -->

            <!-- START Toolbar -->
            <div class="navbar-toolbar clearfix">
                <!-- START Left nav -->
                <ul class="nav navbar-nav navbar-left">
                    <!-- Sidebar shrink -->
                    <li class="hidden-xs hidden-sm">
                        <a href="javascript:void(0);" class="sidebar-minimize" data-toggle="minimize" title="Minimize sidebar">
                            <span class="meta">
                                <span class="icon"></span>
                            </span>
                        </a>
                    </li>
                    <!--/ Sidebar shrink -->

                    <!-- Offcanvas left: This menu will take position at the top of template header (mobile only). Make sure that only #header have the `position: relative`, or it may cause unwanted behavior -->
                    <li class="navbar-main hidden-lg hidden-md hidden-sm">
                        <a href="javascript:void(0);" data-toggle="sidebar" data-direction="ltr" rel="tooltip" title="Menu sidebar">
                            <span class="meta">
                                <span class="icon"><i class="ico-paragraph-justify3"></i></span>
                            </span>
                        </a>
                    </li>
                    <!--/ Offcanvas left -->

					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Shortcuts</a>
							<ul class="dropdown-menu">
								<li><a href="#">Register Manager</a></li>
								<li><a href="#">Register Product</a></li>								
								<li><a href="#">Register Region</a></li>
							</ul>
					</li>
					
                </ul>
                <!--/ END Left nav -->

                <!-- START navbar form -->
                <!-- <div class="navbar-form navbar-left dropdown" id="dropdown-form">
                    <form action="" role="search">
                        <div class="has-icon">
                            <input type="text" class="form-control" placeholder="Search application...">
                            <i class="ico-search form-control-icon"></i>
                        </div>
                    </form>
                </div> -->
                <!-- START navbar form -->

                <!-- START Right nav -->
                <ul class="nav navbar-nav navbar-right">					

                    <!-- Profile dropdown -->
                    <li class="dropdown profile">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                            <span class="meta">
                                <span class="avatar"><img src="../image/avatar/avatar7.jpg" class="img-circle" alt="" /></span>
                                <span class="text hidden-xs hidden-sm pl5">Admin Settings</span>
                            </span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="javascript:void(0);"><span class="icon"><i class="ico-user-plus2"></i></span> My Accounts</a></li>
                            <li><a href="javascript:void(0);"><span class="icon"><i class="ico-cog4"></i></span> Profile Setting</a></li>
                            <li><a href="javascript:void(0);"><span class="icon"><i class="ico-question"></i></span> Help</a></li>                            
                            <li><a href="javascript:void(0);"><span class="icon"><i class="ico-exit"></i></span> Sign Out</a></li>
                        </ul>
                    </li>
                  
                </ul>
                <!--/ END Right nav -->
            </div>
            <!--/ END Toolbar -->
        </header>
        <!--/ END Template Header -->

        <!-- START Template Sidebar (Left) -->
        <aside class="sidebar sidebar-left sidebar-menu">
			<!-- START Sidebar Content -->
            <section class="content slimscroll">
				<!-- START Template Navigation/Menu -->
                <ul class="topmenu topmenu-responsive" data-toggle="menu">                    
                    <li class="active open">
                        <a href="UploadOrderDetails.jsp" data-target="#dashboard" data-parent=".topmenu">
                            <span class="figure"><i class="ico-home2"></i></span>
                            <span class="text">Upload Report</span>
                        </a>
                    </li>
                    <li>
                        <a href="OrdersSummaryPage.jsp" data-target="#dashboard" data-parent=".topmenu">
                            <span class="figure"><i class="ico-stats-up"></i></span>
                            <span class="text">Orders Per Region</span>
                        </a>
                    </li>
                    <li>
                        <a href="ReturnsPerRegion.jsp" data-target="#dashboard" data-parent=".topmenu">
                            <span class="figure"><i class="ico-stats-up"></i></span>
                            <span class="text">Returns Per Region</span>
                        </a>
                    </li>
                    <li>
                        <a href="ProductCategorySalesReportPage.jsp" data-target="#dashboard" data-parent=".topmenu">
                            <span class="figure"><i class="ico-stats-up"></i></span>
                            <span class="text">Product Category Sales</span>
                        </a>
                    </li>
                    <li>
                        <a href="OrdersPerDatePage.jsp" data-target="#dashboard" data-parent=".topmenu">
                            <span class="figure"><i class="ico-stats-up"></i></span>
                            <span class="text">Orders Per Month</span>
                        </a>
                    </li>				
				</ul>
                <!--/ END Template Navigation/Menu -->
            </section>
            <!--/ END Sidebar Container -->
        </aside>
        <!--/ END Template Sidebar (Left) -->
				
        <!-- START Template Main -->
        <section id="main" role="main">
            <!-- START Template Container -->
            <div class="container-fluid">
                <!-- Page Header -->
                <div class="page-header page-header-block">
                    <div class="page-header-section">
                        <h4 class="title semibold">UploadExcelData</h4>
                    </div>
                    <div class="page-header-section">
                        <!-- Toolbar -->
                        <div class="toolbar">
                            <ol class="breadcrumb breadcrumb-transparent nm">
                                <li><a href="#">Upload</a></li>
                                <li class="active">Orders</li>
                            </ol>
                        </div>
                        <!--/ Toolbar -->
                    </div>
                </div>
                <!-- Page Header -->
                <div class="row">
            	<div class="col-md-12">
            		<div class="panel panel-info">
            			<div class="panel-heading">
                                <h3 class="panel-title">Upload Orders Excel Data</h3>
                        </div>
                        <div id="registration_div" class="panel-body">
                        	
													<div id="registration_result"></div>
													<form role='form' id='registration_form' name='registration_form' action='Register_Bulk_Payment_Numbers' method='post'>
														<div class='row col-md-10'>
															<div class='form-group col-md-6'>
																<div class="input-group">
																	<span class="input-group-btn">
																		<span class="btn btn-primary btn-file">Browse&hellip;
																			<input name="excel_file" id="excel_file" type="file">
																		</span>
																	</span>
																	<input type="text" class="form-control" name="excel_file_text" id="excel_file_text" placeholder="Excel sheets with Orders Data" readonly>
																</div>
															</div>
															<div class='form-group col-md-4'>
																<input type="submit" class="btn btn-success" value="Upload"/>
															</div>
														</div>
													</form>							
                        </div>
            		</div>
            	</div>
            </div>                
            </div>
            <!--/ END Template Container -->
            <!-- START To Top Scroller -->
            <a href="#" class="totop animation" data-toggle="waypoints totop" data-showanim="bounceIn" data-hideanim="bounceOut" data-offset="50%"><i class="ico-angle-up"></i></a>
            <!--/ END To Top Scroller -->
            
        </section>
        <!--/ END Template Main -->

        <!-- START JAVASCRIPT SECTION (Load javascripts at bottom to reduce load time) -->
        <!-- Application and vendor script : mandatory -->
        <script type="text/javascript" src="js/vendor.js"></script>
        <script type="text/javascript" src="js/core.js"></script>
        <script type="text/javascript" src="js/app.js"></script>
        <script src="js/jQuery-2.1.4.min.js" type="text/javascript"></script>
        <script src="js/loadover.js"></script>
        <!--/ Application and vendor script : mandatory -->

        <!-- Plugins and page level script : optional -->
		<script type="text/javascript" src="js/pace.min.js"></script>
        <!--/ Plugins and page level script : optional -->
        <!--/ END JAVASCRIPT SECTION -->
        
        <script>
    $(document).on('change', '.btn-file :file', function() {
    	  var input = $(this),
    	      numFiles = input.get(0).files ? input.get(0).files.length : 1,
    	      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    	  input.trigger('fileselect', [numFiles, label]);
    	});

    	$(document).ready( function() {
    	    $('#excel_file').on('fileselect', function(event, numFiles, label) {
    	         //excel_file
    	    $('#excel_file_text').val(label);    	    	
    	    });
    	});
    </script>
    
    <script type="text/javascript">
	$("#registration_form").submit(
			function(event) {
				// Stop form from submitting normally
				event.preventDefault();
				// Get some values from elements on the page:
				var form_data = new FormData($(this)[0]);
				$("#registration_div").loadOverStart();
				$("#bulk_payment_registration_div").hide();
				$("#registration_processing_div").show();				
				$.ajax({
					type : "POST",
					timeout : 2400000,
					url : "HandleOrderExcelReportsServlets",
					data : form_data,
					async : true,
					cache: false,
				    contentType: false,
				    processData: false,
					success : (function(data) {													
						var transaction_response = data["transaction_status"].trim();
						var transaction_description = data["transaction_description"].trim();						
						$("#registration_div").loadOverStop();						
						$("#excel_file_text").val("");
						if(transaction_response=="Transaction success")
						{											
							$("#registration_result").removeClass("alert-danger").removeClass("alert-info").addClass("alert alert-success");
							$("#registration_result").empty().append("<strong>Operation successful, "+transaction_description+"</strong>");
						}
						else if(transaction_response=="Transaction failed")
						{
							$("#registration_result").removeClass("alert-success").removeClass("alert-info").addClass("alert alert-danger");
							$("#registration_result").empty().append("<strong>Operation failed, "+transaction_description+"</strong>");
						}
						else
						{
							$("#registration_result").removeClass("alert-success").removeClass("alert-info").addClass("alert alert-danger");
							$("#registration_result").empty().append("<strong>Operaion Failed</strong>");
						}
					}),
					error : (function(x,t,m) {						
						if(t==="timeout") {
							$("#registration_div").loadOverStop();							
							$("#registration_result").removeClass("alert-success").removeClass("alert-danger").addClass("alert alert-info");
							$("#registration_result").empty().append("<strong>Operation Timed-Out, but still being processed</strong>");	
						}													
					})
				});
				return false;
			});
	</script>
    
    </body>
    <!--/ END Body -->
</html>